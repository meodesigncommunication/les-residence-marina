<?php
/* 
 * Template name: MEO CRM REALESTATE List Lots
 */
global $wpdb;
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$filters = $_SESSION['filters'];
$traductions_file = file_get_contents($plugin_root_uri.'languages.json');
$traductions = json_decode($traductions_file, true);
$floors = RealestateModel::selectFloor(true);
$rooms = RealestateModel::selectLotRooms();
$surfaces = RealestateModel::selectLotSurface();
$buildings = RealestateModel::selectBuildingsWithLot();
$metas = RealestateModel::selectMeta();
$filter_datas = RealestateModel::selectFilterMetaValue();

$menus = array();
$wp_navigations = wp_get_nav_menu_items('main');
foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
        $menus[$nav->ID]['object_id'] = $nav->object_id;
    }
}

$data = array();
$post = new TimberPost();
$data = Timber::get_context();
$data['post'] = $post;
$post = new TimberPost();
$data['post'] = $post;
$data['posts'] = Timber::get_posts();
$data['page'] = 'Liste lots';
$data['plugin_path'] = plugins_url();
$data['type_lot'] = 'Appartements';
$data['metas'] = $metas;
$data['floors'] = $floors;
$data['rooms'] = $rooms;
$data['surfaces'] = $surfaces;
$data['surfaces_filter'] = array(
    array('label' => '25-44m<sup>2</sup>', 'value' => '25-44'),
    array('label' => '45-69m<sup>2</sup>', 'value' => '45-69'),
    array('label' => '70-94m<sup>2</sup>', 'value' => '70-94'),
    array('label' => '95-119m<sup>2</sup>', 'value' => '95-119'),
    array('label' => '&rsaquo;120m<sup>2</sup>', 'value' => '120')
);
$data['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false ;
$data['ajaxurl'] = admin_url('admin-ajax.php');
$data['buildings'] = $buildings['buildings'];
$data['filters'] = $filters;
$data['filter_datas'] = $filter_datas;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['page_list_lot'] = site_url('/meo-crm-realestate-building/');
$data['file_request'] = site_url('/file-request/');
$data['page_lot_details_mobile'] = site_url('/meo-crm-realestate-lot/');
$data['price_enabler'] = MeoScCf7Integration::isPriceEnabled();
$data['devise'] = MEO_CORE_DEVISE;
$data['show_price_lot'] = SHOW_PRICE_LOT; // Set in wp_config.php

$data['options'] = wp_load_alloptions();
$data['template_path'] = get_template_directory();
$data['template_path_uri'] = get_template_directory_uri();
$data['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$data['menus'] = $menus;
$data['current_lang'] = qtranxf_getLanguage();
$data['t'] = $traductions[qtranxf_getLanguage()];
$data['languages'] = qtrans_getSortedLanguages();

// Check data filter
foreach($data['surfaces_filter'] as $key => $value){
    $check = false;
    foreach($surfaces as $surface)
    {
        list($value1, $value2) = explode('-',$value['value']); 
        
        if(isset($value1) && isset($value2))
        {
            if($value1 <= $surface->surface && $value2 >= $surface->surface)
            {
                $check = true;
            }
        }else{
            if($value['value'] <= $surface->surface)
            {
                $check = true;
            }
        }
    }
    if(!$check)
    {
        unset($data['surfaces_filter'][$key]);
    }
}

/*echo '<pre>';
print_r($data['buildings']);
echo '</pre>';
exit();*/

Timber::render('twig/meo-crm-realestate-list-lots.html.twig', $data);