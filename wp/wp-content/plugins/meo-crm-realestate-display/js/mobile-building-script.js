jQuery(function(){
    
    jQuery('.details_building').click(function(){
        if(jQuery(this).parents('.building-details').find('.info-building').css('display') == 'none')
        {
            jQuery(this).parents('.building-details').find('.info-building').slideDown(1000);
            jQuery(this).find('.fa-plus').addClass('hidden');
            jQuery(this).find('.fa-minus').removeClass('hidden');
        }else{
            jQuery(this).parents('.building-details').find('.info-building').slideUp(500);
            jQuery(this).find('.fa-minus').addClass('hidden');
            jQuery(this).find('.fa-plus').removeClass('hidden');
        }
    });
    
    jQuery('.btn-popup-situation.close-popup').click(function(){
        jQuery(this).parents('#popup-vignette').hide();
        jQuery(this).parents('.fancybox-details-lot').hide();
        /*jQuery(this).parents('.building-container').find('.building-content').show();*/
        jQuery(this).parents('.building-container').find('.table-lots').show();
    });
});

function show_lot_plane(element)
{
    var id_show = jQuery(element).attr('id-show');
    
    jQuery(element).parents('.building-container').find('#popup-vignette').find('.detail-popup').each(function(){
        jQuery(this).hide();
    });

    $('html, body').animate({
        scrollTop: $('#header').height()
    },'slow');
    
    jQuery(element).parents('.building-container').find('#popup-vignette').show();
    jQuery(element).parents('.building-container').find('#popup-vignette').find('#details-lot-'+id_show).show();
    /* jQuery(element).parents('.building-container').find('.building-content').hide();*/
    jQuery(element).parents('.building-container').find('.table-lots').hide();
}