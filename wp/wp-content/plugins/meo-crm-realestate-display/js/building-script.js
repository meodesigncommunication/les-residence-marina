$(document).ready(function(e){
    
    $.support.cors = true;
    
    var count = 0;
    
    // Declared Popup Download plan
    $('.smartcapture').fancybox({
        'width': 400,
        'height': 800,
        'autoDimensions': false,
        'type': 'iframe',
        'autoSize': false
    });
    
    $('.details-plan').fancybox({
        autoDimensions: false,
        autoScale: false,
        showCloseButton:false,
        width: 400
    });
    
    // Declared Plugin JS for resize imagemap
    setTimeout(function(){ jQuery('map').imageMapResize(); }, 500);    
    
    // Check building order open
    if($('#selected_building').val() == 0)
    {
        $('.building-container').each(function(){
            if(count <= 0)
            {
                var id = $(this).attr('data-order');
                $(this).show();
                $('#selected_building').val(id);
                change_image_sector(id);
            }
            count += 1;
        });
    }else{
        change_image_sector($('#selected_building').val());
    }
    
    // Select another building building
    var count = 1;
    var total = $('.building-container').length;
    
    $('.building-container').each(function(){
        $(this).attr('data-count', count);
        if(count == 1)
        {
            $(this).find('.previous-building').attr('onclick','changeBuildingBtn('+total+',this)');
            $(this).find('.previous-building').attr('data-building',total);
            $(this).find('.next-building').attr('onclick','changeBuildingBtn('+((count*1)+1)+',this)');
            $(this).find('.next-building').attr('data-building',((count*1)+1));
        }else if(count == total)
        {
            $(this).find('.previous-building').attr('onclick','changeBuildingBtn('+((count*1)-1)+',this)');
            $(this).find('.previous-building').attr('data-building',((count*1)-1));
            $(this).find('.next-building').attr('onclick','changeBuildingBtn(1,this)');
            $(this).find('.next-building').attr('data-building','1');
        }else{
            $(this).find('.previous-building').attr('onclick','changeBuildingBtn('+((count*1)-1)+',this)');
            $(this).find('.previous-building').attr('data-building',((count*1)-1));
            $(this).find('.next-building').attr('onclick','changeBuildingBtn('+((count*1)+1)+',this)');
            $(this).find('.next-building').attr('data-building',((count*1)+1));
        }
        count++;
    });

    /* ROTATION BUILDING */
    $('.rotate-building').click(function(){                
        var display = $('.front-building').css('display');
        if(display == 'block')
        {
            $('.front-building').hide();
            $('.back-building').show();
        }else{                    
            $('.front-building').show();
            $('.back-building').hide();
        }
        jQuery('map').imageMapResize();
    });
    
    /*$(".building-lot-selector").on("swipeleft",function(){
        var current_building = $(this).find('.previous-building-mobile').attr('data-building');
        $(this).find('.building-effect').fadeOut('slow',function(){
            changeBuilding(current_building);               
            $('.building-container[data-count="'+current_building+'"]').find('.building-effect').fadeIn('slow');            
        });        
    });
    
    $(".building-lot-selector").on("swiperight",function(){
        var current_building = $(this).find('.next-building-mobile').attr('data-building');        
        $(this).find('.building-effect').fadeOut('slow',function(){
            changeBuilding(current_building);               
            $('.building-container[data-count="'+current_building+'"]').find('.building-effect').fadeIn('slow');            
        }); 
    });*/
});

// Select lot on building and show in table a selected lot
$('.lot-building-list').click(function(e){
    e.preventDefault();
    var id = $(this).attr('data-id');     
    var image_hover = $(this).attr('data-id-image');
    var color = $(this).attr('data-color');                
    var lot = $('tr#lot_list_'+id).html();
    var html_lot_list = $(this).parents('.building-container').find('.selected-lot-list').html();         
    if(html_lot_list != '')
    {        
        $(this).parents('.building-container').find('.selected-lot-list').fadeOut(500, function(){
            $(this).parents('.building-container').find('.selected-lot-list').html(lot);
        });
    }else{
        $(this).parents('.building-container').find('.selected-lot-list').html(lot);
    }    
    $(this).parents('.building-container').find('.selected-lot-list').fadeIn(1000, function(){initFancyboxElement()});  
    
    $(this).parent().parent().find('.hover_status_lot').hide();
    $(this).parent().parent().find('.status_lot_hover_'+id).show();
    
    initFancyboxElement();
    console.log('test test test');
});

/* CHANGE BUILDING */
$('.map_realestate_sector area').click(function(e){
    e.preventDefault();
    var element_visible = $(this).attr('data-building');
    var id_building = $(this).attr('data-id');
    $('#selected_building').val(id_building);    
    $('.building-container').hide();
    $('#'+element_visible).show();
    $('#'+element_visible).find('.building-content').show();
    jQuery('map').imageMapResize();
    change_image_sector(id_building);
    $('.btn-list-lot').fancybox(); 
    initFancyboxElement();
});  

function changeBuildingBtn(id,elem)
{
    $(elem).parents('.building-content').fadeOut('fast',function(){
        changeBuilding(id);               
        $('.building-container[data-count="'+id+'"]').find('.building-content').fadeIn('fast');
    }); 
    jQuery('map').imageMapResize();
}

function changeBuilding(id)
{
    $('article.building-container').each(function(){
        $(this).css('display','none');
    });   
    if(id != 1)
    {
        $('building_full_content_1').css('display','none');
    }
    $('article.building-container[data-count='+id+']').show();    
    var id_selected = $('article.building-container[data-count='+id+']').attr('data-order'); 
    $('article.building-container[data-count='+id+']').find('.building-effect').show();
    $('#selected_building').val(id_selected);
    change_image_sector(id_selected);
    setTimeout(function(){ jQuery('map').imageMapResize(); }, 500); 
}

function change_image_sector(id)
{    
    $('.hover-sector-2d').hide();
    $('#building_'+id+'_'+id).show();
    jQuery('map').imageMapResize();
    initFancyboxElement();
}
function initFancyboxElement()
{
    setTimeout(function(){
        $('.smartcapture').fancybox({
            'width': 400,
            'height': 800,
            'autoDimensions': false,
            'type': 'iframe',
            'autoSize': false

        });       
        $('.details-plan').fancybox({
            autoDimensions: false,
            autoScale: false,
            showCloseButton:false,
            width: 400
        }); 
    }, 1000);
}
