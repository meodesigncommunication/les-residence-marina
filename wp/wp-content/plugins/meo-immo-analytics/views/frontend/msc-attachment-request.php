<?php
    /**
     * MSC Attachment Request
     */

    $body_class = '';

    $file_id = (isset($_GET['file_id']) && !empty($_GET['file_id'])) ? (int) $_GET['file_id'] : 0;
    $post_id = (isset($_GET['post_id']) && !empty($_GET['post_id'])) ? (int) $_GET['post_id'] : 0;
    $lot_id = (isset($_GET['lot_id']) && !empty($_GET['lot_id'])) ? (string) $_GET['lot_id'] : '';


    // Get lot data if lot_id exists
    if(isset($lot_id) && !empty($lot_id)){

        $lot = RealestateModel::selectLotById($lot_id);
        $_GET['item'] = $lot['title'];

    }else if(isset($post_id) && !empty($post_id)){

        $post = get_post($post_id);
        $_GET['item'] = $post->post_name;

    }else if(isset($file_id) && !empty($file_id)){

        $id_decode = ($file_id-3)/17 ;
        $file = get_post($id_decode);
        $_GET['item'] = $file->post_name;

    }else{
        $_GET['item'] = '';
    }

    /*if (!empty($lot)) {
        if (!empty($lot['type_lot']) && !empty($lot['type']['slug'])) {
            $body_class = 'download-lot-type-' . $lot['type']['slug'];
        }
    }
    else {
        $page = get_post($post_id);
        if (!empty($page)) {
            $body_class = 'download-page-' . $page->post_name;
        }
    }*/

?><!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js html-file-request" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <!--[if lte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=IE8" /><![endif]-->
    <title><?php wp_title( '' ); // wp_title is filtered by includes/customizations.php risen_title() ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" />


    <script src="<?php echo site_url(); ?>/wp-content/themes/meo-theme-immobilier-steiner/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $ = jQuery.noConflict();
    </script>
    <link rel='stylesheet' id='meo_crm_core_colorpicker_layout_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/meo-crm-core/js/colorpicker/css/layout.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='meo_crm_core_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/meo-crm-core/css/core.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='meo_crm_core_google_css-css'  href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css'  href='<?php echo site_url(); ?>/wp-includes/css/dashicons.min.css?ver=4.6.2' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='<?php echo site_url(); ?>/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.5.1' type='text/css' media='all' />
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/meo-immo-analytics/js/jquery.cookie.js?ver=1.4.1'></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/meo-immo-analytics/js/meo-smart-capture.js?ver=1.0.0'></script>
    <link rel='https://api.w.org/' href='<?php echo site_url(); ?>-json/' />

    <!-- Piwik -->
    <script type="text/javascript">
        var analytics_post_id = 10,
            analytics_post_type = 'page',
            analytics_prices = 'false';

        var triesCounter = 0;
        piwikload = function() {
            if (typeof jQuery === "undefined") {
                triesCounter++;
                if (triesCounter < 20) {
                    setTimeout(piwikload, 50);
                }
            }
            else {
                var $forms = jQuery('form.wpcf7-form');
                if ($forms.length !== 0) {
                    $forms.each(function () {
                        jQuery('<input>').attr({
                            type: 'hidden',
                            name: 'analytics_id',
                            value: "" + piwik_user_id
                        }).appendTo(jQuery(this));
                    });
                }
            }
        };
        var _paq = _paq || [];
        _paq.push(['setCustomVariable', '1', 'post_id',        analytics_post_id,   "page"]);
        _paq.push(['setCustomVariable', '2', 'post_type',      analytics_post_type, "page"]);
        _paq.push(['setCustomVariable', '3', 'prices_enabled', analytics_prices,    "page"]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        _paq.push([ function() {
            piwik_user_id = this.getVisitorId();
            piwikload();
        }]);
        (function() {
            var u=(("https:" == document.location.protocol) ? "https" : "http") + "://piwik.meoanalytics.com/";
            _paq.push(['setTrackerUrl', u+'piwik.php']);
            _paq.push(['setSiteId', 32670475]);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
            g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
    </script>
    <noscript><p><img src="http://piwik.meoanalytics.com/piwik.php?idsite=32670475" style="border:0;" alt="" /></p></noscript>
    <!-- End Piwik Code -->


    <?php
    meo_immo_analytics_google_tracking_code();
     ?>
</head>
<body <?php body_class($body_class); ?>>
    <div id="smartcapture-form">
	<?php
	while ( have_posts() ) : the_post();
		the_content();
	endwhile;
	?>
    </div>

    <script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/meo-theme-immobilier-steiner/js/steiner-script.js"></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpcf7 = {"loaderUrl":"http:\/\/meomeo.ch\/wp\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptcha":{"messages":{"empty":"Merci de confirmer que vous n\u2019\u00eates pas un robot."}},"sending":"Envoi en cours..."};
        /* ]]> */
    </script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.5.1'></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-includes/js/wp-embed.min.js?ver=4.6.2'></script>
</body>
</html>