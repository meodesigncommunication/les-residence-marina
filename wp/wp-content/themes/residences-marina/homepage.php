<?php
/*
 * Template name: TPL Homepage
 */

/* BASE CONTEXT ALL PAGE INCLUDE */
$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['charset'] = 'UTF-8';
$context['title'] = $post->post_title;
$context['content'] = $post->post_content;
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$context['menus'] = wp_get_nav_menu_items('main');
$context['current_lang'] = qtranxf_getLanguage();
$context['languages'] = qtrans_getSortedLanguages();
/* / BASE CONTEXT ALL PAGE INCLUDE */

/* PAGE CONTEXT DATA */
//$context['title_homepage'] = __('[:fr]Une qualité de vie exceptionnelle[:en]An exceptional quality of life[:de]Hervorragende Lebensqualität');
$context['title_homepage'] = 'Une qualité de vie<br/>exceptionnelle';
$context['sliders'] = (have_rows('sliders')) ? get_field('sliders') : array();
$context['menus_homepage'] = wp_get_nav_menu_items('home');
/* / PAGE CONTEXT DATA */

/*echo '<pre>';
print_r($context['menus_homepage']);
echo '</pre>';
*/

Timber::render( 'templates/homepage.html.twig' , $context );