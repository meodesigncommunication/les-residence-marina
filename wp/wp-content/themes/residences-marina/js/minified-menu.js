/**
 * Created by MEO on 11.04.2017.
 */
$(function(){
    $(document).ready(function(){
        $('nav').after().click(function(e){
            if($('nav ul').css('display') == 'none') {
                $('nav ul').addClass('minified');
                $('nav ul').toggle( "fast");
            }else if($('nav ul').css('display') == 'block' && $('nav ul').hasClass('minified')){
                $('nav ul').removeClass('minified');
                $('nav ul').toggle( "fast");
            }
        });
    });
});