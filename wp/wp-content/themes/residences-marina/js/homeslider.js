/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){
	
	var $slides = $("#slider .slide");        
	//var $slidesTXT = $("#slider-txt .slide-txt");
 	var currentSlide = 0;	
	var stayTime = 6;	
	var slideTime = 2.5;
				
	TweenLite.set($slides.filter(":gt(0)"), {scale: 1, autoAlpha:0});
	TweenLite.delayedCall(stayTime, nextSlide);			
				
	function nextSlide()
    {	
            TweenLite.to( $slides.eq(currentSlide), slideTime, {scale: 1, autoAlpha:0} );		
            //TweenLite.to( $slidesTXT.eq(currentSlide), slideTime, {autoAlpha:0} );
            currentSlide = ++currentSlide % $slides.length;
            TweenLite.to( $slides.eq(currentSlide), slideTime, {scale: 1, autoAlpha:1} );
            //TweenLite.to( $slidesTXT.eq(currentSlide), slideTime, {autoAlpha:1} );
            TweenLite.delayedCall(stayTime, nextSlide);	
	}

});

