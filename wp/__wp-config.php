<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

define( 'AUTOMATIC_UPDATER_DISABLED', true );

define('DB_NAME', 'rml_marina');
define('DB_USER', 'rml_marina');
define('DB_PASSWORD', 'f7NfJ1H_PG87');
define('DB_HOST', 'rml.myd.sharedbox.com');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

define('IS_PRODUCTION', true);

define('SHOW_PRICE_LOT', 0);

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y}uA[9g2UPB /u>:EkP%G(>nt_E+>D28@^MEUiK9iC12r].A)kjEQb=/xxZ:1k4f');
define('SECURE_AUTH_KEY',  '6|RG#Bv&l9:f1&|5FM_w_D_f8ulAoP*[t19u5vIBW=L<zAzxPefjN;MS*{i-V@iL');
define('LOGGED_IN_KEY',    'YGcE<eHm6YIr+Wz>~-[Klv[XAt2-Wo^&IO4!.!x(Ff-cY*Dl=o&WNOfm9=MKk+`%');
define('NONCE_KEY',        'R!<^VrW6=F(IlZLh$vV&)po*t!Sb9KBPbJ<[mt%._1to.=5FFXZZzkV9_}7%>8s~');
define('AUTH_SALT',        'ZUH,BQa3zMN5)K,=nGi-je@+%{-/y!o-nF`Mk3aYZJOmExPg)lqaO/nfs;mP@j~G');
define('SECURE_AUTH_SALT', '/maKaqC+F=:RMtcBR:CH|R~M{Gos@<1u+ZMBR5?Oin2<@l1r~zq2EQ2kNg$5C`gn');
define('LOGGED_IN_SALT',   '(;]>!E1rM2i*O.0-yF !1J?ZcI72?-@0+)hPW@x3gKFWyY)=0qicGO7e@=vZ$8.f');
define('NONCE_SALT',       'Wq|>[T,8JpDwC-B;P6[_qr2?3.jK-~.cEO#<#.E6TXN|E0WRfWMf^G.G[GHK(sqM');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d'information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');