<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

define( 'AUTOMATIC_UPDATER_DISABLED', true );

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'rml_marina');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'rml_marina');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'f7NfJ1H_PG87');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'rml.myd.sharedbox.com');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

define('IS_PRODUCTION', true);

define('SHOW_PRICE_LOT', 0);

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<P?,/(;pF3J.=e~wMx f-n1SZ-:OH6L~#Ehc1oTp]4yA_3V>WA>Jd(_B*JYLS?[+');
define('SECURE_AUTH_KEY',  'FR.jgz/Os>g>eT_snofQBwO>hh=8DA&{n$a;SfckPadZ2ri,<s9~UvAz0j@iX(w^');
define('LOGGED_IN_KEY',    '>}<l;eoCvl_Yg@V?i^I SUziX{_qjc%Pw~sBfG;}c^}F:M}?X,CxA:[f-X:%<uhq');
define('NONCE_KEY',        'Wl@L0m8<Y{,fKMhAzQ#7DaV 4-8_KKsQfV73K7GTXb5C^tPFPaWJ]CJ$s1cAY7%5');
define('AUTH_SALT',        'Xd?;]mrf|=:Y_?b53 *i^LbsNu!%=bT>WR0zf`B8F|j~]a}rq03.qBE6)Fr3^^%|');
define('SECURE_AUTH_SALT', 'A!jW@5WVPR6%hA@/)j^ssi$nkI=ZP%m;3y~yo~;#<9sk}eASl:i2H] yo{O<P8la');
define('LOGGED_IN_SALT',   'wKQ!,] &T-F@)Ae?w:ApUzjwL.V[tCqeMJO<#g%6.`4lpsX:6Z0/{l5r@p>p-FDr');
define('NONCE_SALT',       'ctsK/6n`p8-~DI?7F[<7_0EOJi9(jmYuA,Y TJ{@DYVfUyb.B5qi lXg~Dyv_qV7');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');