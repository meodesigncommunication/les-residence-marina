<?php date_default_timezone_set("Europe/Zurich");

/*
Plugin Name: MEO CRM Core
Description: Core Plugin MEO CRM site
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

$wpUploadsDir = wp_upload_dir();
define('MEO_CORE_PLUGIN_SLUG', 		'meo-crm-core');
define('GOOGLE_CORE_CSS', 			'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
define('GOOGLE_CORE_JS', 			'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
define('GOOGLE_CORE_CLIENT_ID', 	'378538563046-fdge1tn1otpg8km2rijhe1034ik5n0d6.apps.googleusercontent.com');
define('MEO_CORE_MAIL_NAME', 		'MEO Real Estate - SmartCapture');
define('MEO_CORE_MAIL_USER_FROM', 	'no-reply@smartcapture.ch');
define('MEO_CORE_MAIL_USER_TO', 	'digital@meomeo.ch');

define('MEO_CORE_DEVISE', 	'CHF');

# Librairies
require_once( $plugin_root . "librairies/fpdf/fpdf.php");
require_once( $plugin_root . 'librairies/fpdf/fpdi.php');

# Classes
require_once( $plugin_root . 'classes/meo-crm-core-tools.php');
require_once( $plugin_root . 'classes/meo-crm-core-templater.php');
require_once( $plugin_root . 'classes/meo-crm-core-crypt-data.php');
require_once( $plugin_root . 'classes/meo-crm-core-mobile-detect.php');
require_once( $plugin_root . 'classes/meo-crm-core-file-manager.php');
require_once( $plugin_root . 'classes/meo-crm-core-shortcode.php');
require_once( $plugin_root . 'classes/meo-crm-core-locator.php');
require_once( $plugin_root . 'classes/meo-crm-core-helper.php' );
require_once( $plugin_root . 'classes/meo-crm-core-validation-form.php' );
$MeoCrmCoreLocator = new MeoCrmCoreLocator();
$MeoCrmCoreShortcode = new MeoCrmCoreShortcode();

add_shortcode( 'meo_crm_core_previous_page' , array( $MeoCrmCoreLocator, 'getButtonPreviousPage' ) );


# Tools
add_action( 'activated_plugin', 'meo_crm_core_load_first' );
// TODO::Maybe add also when plugins loaded
function meo_crm_core_load_first() {
	$path = str_replace( WP_PLUGIN_DIR . '/', '', __FILE__ );
	if ( $plugins = get_option( 'active_plugins' ) ) {
		if ( $key = array_search( $path, $plugins ) ) {
			array_splice( $plugins, $key, 1 );
			array_unshift( $plugins, $path );
			update_option( 'active_plugins', $plugins );
		}
	}
}

function meo_crm_core_slug_exists($slug) {
	global $wpdb;
	if($wpdb->get_row("SELECT post_name FROM ".$wpdb->prefix."posts WHERE post_name = '" . $slug . "'", 'ARRAY_A')) {
		return true;
	} else {
		return false;
	}
}

function meo_crm_core_is_mobile() {
	$detect = new Mobile_Detect;
	return $detect->isMobile();
}

function meo_crm_core_is_tablet() {
	$detect = new Mobile_Detect;
	return $detect->isTablet();
}

add_action('init', 'meo_crm_core_StartSession', 1);

function meo_crm_core_StartSession() {
	if(!session_id()) {
		session_start();
	}
}

# Add Scripts and Styles

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script_style_core' );
function load_custom_wp_admin_script_style_core() {

        # JS
        wp_register_script( 'threesixty-spinner-jquery',  plugins_url('js/threesixty-spinner-jquery.js', __FILE__), false, '1.0.0', true );
        wp_enqueue_script( 'threesixty-spinner-jquery' );
        

        # CSS
        wp_register_style( 'meo_crm_core_css',  plugins_url('css/core.css', __FILE__), false, '1.0.0');
        wp_enqueue_style( 'meo_crm_core_css' );
        
        
        // Google
        wp_register_script( 'meo_crm_core_google_js',  GOOGLE_CORE_JS, false, '1.0.0' );
        wp_enqueue_script( 'meo_crm_core_google_js' );
        wp_register_style( 'meo_crm_core_google_css',  GOOGLE_CORE_CSS, false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_core_google_css' );
        
        
        // Table Sorter
        wp_register_script( 'meo_crm_core_tablesorter_js',  plugins_url('js/jquery.tablesorter/jquery.tablesorter.min.js', __FILE__), false, '1.0.0' );
        wp_enqueue_script( 'meo_crm_core_tablesorter_js' );
        wp_register_script( 'meo_crm_core_tablesorter_pager_js',  plugins_url('js/jquery.tablesorter/jquery.tablesorter.pager.js', __FILE__), false, '1.0.0' );
        wp_enqueue_script( 'meo_crm_core_tablesorter_pager_js' );
		wp_register_style( 'meo_crm_contacts_tablesorter_css',  plugins_url('js/jquery.tablesorter/themes/blue/style.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_contacts_tablesorter_css' );

		// Helper list
		wp_register_script( 'meo_crm_core_helper_list_js',  plugins_url('js/meo-crm-core-helper-list.js', __FILE__), false, '1.0.0', true );
		wp_enqueue_script( 'meo_crm_core_helper_list_js' );
		wp_register_style( 'meo_crm_core_helper_list_css',  plugins_url('css/meo-crm-core-helper-list.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_core_helper_list_css' );
		
        
        
        # COLOR PICKER
        wp_register_script( 'meo_crm_core_colorpicker_colorpicker_js',  plugins_url('js/colorpicker/js/colorpicker.js', __FILE__), false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_core_colorpicker_colorpicker_js' );
        wp_register_script( 'meo_crm_core_colorpicker_eye_js',  plugins_url('js/colorpicker/js/eye.js', __FILE__), false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_core_colorpicker_eye_js' );
        wp_register_script( 'meo_crm_core_colorpicker_utils_js',  plugins_url('js/colorpicker/js/utils.js', __FILE__), false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_core_colorpicker_utils_js' );
        /*wp_register_script( 'meo_crm_core_colorpicker_layout_js',  plugins_url('js/colorpicker/js/layout.js', __FILE__), false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_core_colorpicker_layout_js' );*/
        wp_register_style( 'meo_crm_core_colorpicker_colorpicker_css',  plugins_url('js/colorpicker/css/colorpicker.css', __FILE__), false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_core_colorpicker_colorpicker_css' );
        wp_register_style( 'meo_crm_core_colorpicker_layout_css',  plugins_url('js/colorpicker/css/layout.css', __FILE__), false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_core_colorpicker_layout_css' );
        
        # FANCYBOX

        wp_register_script('meo_crm_core_mousewheel_script',plugins_url('librairies/fancybox/lib/jquery.mousewheel-3.0.6.pack.js', __FILE__), false,'1.0.0');
        wp_enqueue_script( 'meo_crm_core_mousewheel_script' );
        
        wp_register_script('meo_crm_core_fancybox_script',plugins_url('librairies/fancybox/source/jquery.fancybox.pack.js', __FILE__), false,'1.0.0');
        wp_enqueue_script( 'meo_crm_core_fancybox_script' );
        
        wp_register_script('meo_crm_core_fancybox_buttons_script',plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-buttons.js', __FILE__), false,'1.0.0');
        wp_enqueue_script( 'meo_crm_core_fancybox_buttons_script' );
        
        wp_register_script('meo_crm_core_fancybox_thumbs_script',plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-thumbs.js', __FILE__), false,'1.0.0');
        wp_enqueue_script( 'meo_crm_core_fancybox_thumbs_script' );
        
        wp_register_script('meo_crm_core_fancybox_media_script',plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-media.js', __FILE__), false,'1.0.0');
        wp_enqueue_script( 'meo_crm_core_fancybox_media_script' );

        wp_register_style( 'meo_crm_core_fancybox_css',  plugins_url('librairies/fancybox/source/jquery.fancybox.css', __FILE__), false, '1.0.0');
        wp_enqueue_style( 'meo_crm_core_fancybox_css' );
        
        wp_register_style( 'meo_crm_core_fancybox_button_css',  plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-buttons.css', __FILE__), false, '1.0.0');
        wp_enqueue_style( 'meo_crm_core_fancybox_button_css' );
        
        wp_register_style( 'meo_crm_core_fancybox_thumbs_css',  plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-thumbs.css', __FILE__), false, '1.0.0');
        wp_enqueue_style( 'meo_crm_core_fancybox_thumbs_css' );
}

/*
 *  Load Files For Fancybox Plugin
 */
add_action( 'wp_enqueue_scripts', 'load_custom_wp_script_style_core' );
function load_custom_wp_script_style_core() {

	# JS
    wp_register_script( 'meo_crm_core_google_js',  GOOGLE_CORE_JS, false, '1.0.0' );
    wp_enqueue_script( 'meo_crm_core_google_js' );
    
	wp_register_script('meo_crm_core_mousewheel_script', plugins_url('librairies/fancybox/lib/jquery.mousewheel-3.0.6.pack.js', __FILE__), false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_mousewheel_script' );

	wp_register_script('meo_crm_core_fancybox_script', plugins_url('librairies/fancybox/source/jquery.fancybox.pack.js', __FILE__), false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_script' );

	wp_register_script('meo_crm_core_fancybox_buttons_script', plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-buttons.js', __FILE__), false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_buttons_script' );

	wp_register_script('meo_crm_core_fancybox_thumbs_script', plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-thumbs.js', __FILE__), false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_thumbs_script' );

	wp_register_script('meo_crm_core_fancybox_media_script', plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-media.js', __FILE__), false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_media_script' );
	
	# COLOR PICKER
	wp_register_script( 'meo_crm_core_colorpicker_colorpicker_js',  plugins_url('js/colorpicker/js/colorpicker.js', __FILE__), false, '1.0.0', true );
	wp_enqueue_script( 'meo_crm_core_colorpicker_colorpicker_js' );
	wp_register_script( 'meo_crm_core_colorpicker_eye_js',  plugins_url('js/colorpicker/js/eye.js', __FILE__), false, '1.0.0', true );
	wp_enqueue_script( 'meo_crm_core_colorpicker_eye_js' );
	wp_register_script( 'meo_crm_core_colorpicker_utils_js',  plugins_url('js/colorpicker/js/utils.js', __FILE__), false, '1.0.0', true );
	wp_enqueue_script( 'meo_crm_core_colorpicker_utils_js' );
	/*wp_register_script( 'meo_crm_core_colorpicker_layout_js',  plugins_url('js/colorpicker/js/layout.js', __FILE__), false, '1.0.0', true );
	 wp_enqueue_script( 'meo_crm_core_colorpicker_layout_js' );*/
	wp_register_style( 'meo_crm_core_colorpicker_colorpicker_css',  plugins_url('js/colorpicker/css/colorpicker.css', __FILE__), false, '1.0.0' );
	wp_enqueue_style( 'meo_crm_core_colorpicker_colorpicker_css' );
	wp_register_style( 'meo_crm_core_colorpicker_layout_css',  plugins_url('js/colorpicker/css/layout.css', __FILE__), false, '1.0.0' );
	wp_enqueue_style( 'meo_crm_core_colorpicker_layout_css' );

	# CSS
     wp_register_style( 'meo_crm_core_css', plugins_url('css/core.css', __FILE__), false, '1.0.0');
    wp_enqueue_style( 'meo_crm_core_css' );
    
	wp_register_style( 'meo_crm_core_google_css',  GOOGLE_CORE_CSS, false, '1.0.0' );
	wp_enqueue_style( 'meo_crm_core_google_css' );
	
	wp_register_style( 'meo_crm_core_fancybox_css', plugins_url('librairies/fancybox/source/jquery.fancybox.css', __FILE__), false, '1.0.0');
	wp_enqueue_style( 'meo_crm_core_fancybox_css' );

	wp_register_style( 'meo_crm_core_fancybox_button_css', plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-buttons.css', __FILE__), false, '1.0.0');
	wp_enqueue_style( 'meo_crm_core_fancybox_button_css' );

	wp_register_style( 'meo_crm_core_fancybox_thumbs_css', plugins_url('librairies/fancybox/source/helpers/jquery.fancybox-thumbs.css', __FILE__), false, '1.0.0');
	wp_enqueue_style( 'meo_crm_core_fancybox_thumbs_css' );


	// Enqueue Dashicons style for frontend use
	wp_enqueue_style( 'dashicons' );
}

# PAGES & TEMPLATES
	
	# Register Plugins Pages
	add_action( 'plugins_meo_crm_active_create_page', 'meo_crm_core_pages_register' );
	
	function meo_crm_core_pages_register() {
		
		# Call for plugins pages
		$pluginsPages = array();
		$pluginsPages = apply_filters('meo_crm_core_front_pages_collector', $pluginsPages);
			
		# Create Pages for Plugins
		if(is_array($pluginsPages)){
			
			foreach($pluginsPages as $pluginSlug => $pluginPage){
				
				if(!meo_crm_core_slug_exists($pluginSlug)){
										
					$exports_page = array(
							'post_title' => $pluginPage['post_title'],
							'post_content' => $pluginPage['post_content'],
							'post_status' => 'publish',
							'post_type' => 'page',
							'post_author' => 1,
							'post_date' => date('Y-m-d H:i:s'),
							'post_name' => $pluginSlug,
							'meta_input' => array('_wp_page_template' => $pluginPage['_wp_page_template'])
					);
				
					wp_insert_post($exports_page);
				}
				
			}
			
		}
				
			
	}
	
	// TODO::ADD Core admin page to store wp_option values for core mechanism