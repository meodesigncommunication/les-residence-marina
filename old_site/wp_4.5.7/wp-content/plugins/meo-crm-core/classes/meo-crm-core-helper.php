<?php

/*
 * 
 */
class MeoCrmCoreHelper
{
    /*
     *  Generate a flash message for the administration
     * 
     *  @param $message(string), $type(integer) => 0 = erreur & 1 = valide
     *  @return $flash_message(string) => html of flash message 
     */
    public function getFlashMessageAdmin($message,$type)
    {
        if($type)
        {
            $class = 'updated notice is-dismissible';
        }else{
            $class = 'error';
        }
        $flash_message   = '';
        $flash_message  .= '<div class="'.$class.'" style="margin-left: 0px !important">';
        $flash_message  .=      '<p>';
        $flash_message  .=          $message;
        $flash_message  .=      '</p>';
        $flash_message  .= '</div>';
        
        return $flash_message;
    }
    
    /*
     *  Convert a date to data base date
     * 
     *  @param $message(string), $type(integer) => 0 = erreur & 1 = valide
     *  @return $flash_message(string) => html of flash message 
     */
    public static function convertDateFormatToDbFormat($date,$format,$withHours=false,$hours='00:00:00')
    {
        switch($format)
        {
            case 'd/m/y':
                list($day,$month,$year) = explode('/',$date);
                break;
            
            case 'm/d/y':
                list($month,$day,$year) = explode('/',$date);
                break;
            
            case 'd.m.y':
                list($day,$month,$year) = explode('.',$date);
                break;
            
            case 'm.d.y':
                list($month,$day,$year) = explode('.',$date);
                break;
            
            case 'd-m-y':
                list($day,$month,$year) = explode('-',$date);
                break;
            
            case 'm-d-y':
                list($month,$day,$year) = explode('-',$date);
                break;
        }
        
        if($withHours)
        {
            $dateConvert = $year.'-'.$month.'-'.$day.' '.$hours;
        }else{
            $dateConvert = $year.'-'.$month.'-'.$day.'';
        }
                
        return $dateConvert;
    }
}

