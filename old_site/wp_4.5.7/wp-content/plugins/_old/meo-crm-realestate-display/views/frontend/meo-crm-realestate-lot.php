<?php
/* 
 * Template name: MEO CRM REALESTATE Lot
 */
global $wpdb;
$lot_id = $_GET['id'];
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$lot = RealestateModel::selectLotById($lot_id); // get lot datas


$wp_navigations = wp_get_nav_menu_items('main');

foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
        $menus[$nav->ID]['object_id'] = $nav->object_id;
    }
}

$data = array();
$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['page'] = 'Lot detail';
$data['plugin_path'] = plugins_url();
$data['lot'] = $lot;
$data['charset'] = 'UTF-8';
$data['options'] = wp_load_alloptions();
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['previous_page'] = site_url('list-lots');
$data['template_path'] = get_template_directory_uri();
$data['template_path_uri'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['is_mobile'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$data['menus'] = $menus;
$data['current_lang'] = 'fr';

Timber::render('twig/meo-crm-realestate-lot.html.twig', $data);