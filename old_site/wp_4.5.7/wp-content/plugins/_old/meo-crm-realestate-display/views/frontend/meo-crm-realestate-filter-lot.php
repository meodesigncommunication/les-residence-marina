<?php
/* 
 * 
 */
global $wpdb;
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$filters = $_SESSION['filters'];
$floors = RealestateModel::selectFloor(true);
$rooms = RealestateModel::selectLotRooms();
$buildings = RealestateModel::selectBuildingsWithLot();
$metas = RealestateModel::selectMeta();
$filter_datas = RealestateModel::selectFilterMetaValue();

$wp_navigations = wp_get_nav_menu_items('main');

foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
        $menus[$nav->ID]['object_id'] = $nav->object_id;
    }
}

$data = array();
$data = Timber::get_context();
$post = new TimberPost();
$data['post'] = $post;
$data['posts'] = Timber::get_posts();
$data['page'] = 'Liste lots';
$data['plugin_path'] = plugins_url();
$data['type_lot'] = 'Appartements';
$data['metas'] = $metas;
$data['floors'] = $floors;
$data['rooms'] = $rooms;
$data['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false ;
$data['ajaxurl'] = admin_url('admin-ajax.php');
$data['buildings'] = $buildings['buildings'];
$data['filters'] = $filters;
$data['filter_datas'] = $filter_datas;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['page_list_lot'] = site_url('/list-lots-building/');

$data['charset'] = 'UTF-8';
$data['options'] = wp_load_alloptions();
$data['template_path'] = get_template_directory_uri();
$data['template_path_uri'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['is_mobile'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$data['menus'] = $menus;
$data['current_lang'] = 'fr';

Timber::render('twig/meo-crm-realestate-filter-list-lots.html.twig', $data);