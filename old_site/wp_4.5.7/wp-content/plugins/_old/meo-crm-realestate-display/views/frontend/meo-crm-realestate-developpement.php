<?php
/* 
 * Template name: MEO CRM REALESTATE Developpement
 */

global $wpdb;

$upload_path = wp_upload_dir();

$wp_navigations = wp_get_nav_menu_items('main');

foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
        $menus[$nav->ID]['object_id'] = $nav->object_id;
    }
}

// GET DEVELOPPEMENT
$developpements = RealestateModel::selectDeveloppement();

// GET SECTOR
$sectors = RealestateModel::selectSector();

// DECLARE TWIG VARIABLES
$data = Timber::get_context();
$data['developpements'] = $developpements;
$data['sectors'] = $sectors;
$data['page'] = 'Developpement';
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

$data['charset'] = 'UTF-8';
$data['options'] = wp_load_alloptions();
$data['template_path'] = get_template_directory_uri();
$data['template_path_uri'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['is_mobile'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$data['menus'] = $menus;
$data['current_lang'] = 'fr';

Timber::render('twig/meo-crm-realestate-developpement.html.twig', $data);