jQuery(function(){
    jQuery('span.mobile-btn-situation').click(function(){
        var id_show = jQuery(this).attr('id-show');
        var base_height = jQuery('#list-lot-container').height();
        jQuery('#base_height').val(base_height+'px');
        jQuery(this).parents('#list-lot-container').find('#group-lot-situation').find('.lot-content').each(function(){
            jQuery(this).hide();
        });
        jQuery('html, body').animate({
            scrollTop: $('#header').height()
        },'slow');
        jQuery(this).parents('#list-lot-container').parent().find('#group-lot-situation').show();
        jQuery(this).parents('#list-lot-container').parent().find('#group-lot-situation').find('#situation_'+id_show).show();
        var popup_height = jQuery(this).parents('#list-lot-container').parent().find('#group-lot-situation').find('#situation_'+id_show).height();
        jQuery('#list-lot-container').height(popup_height+'px');
        jQuery(this).parents('.vignette-lots').hide();

    });
});
function closeSituation(){
    var height = jQuery('input#base_height').val();
    jQuery('.lot-content').hide();
    jQuery('.vignette-lots').show();
    jQuery('#global-content-list-lot').show();
    jQuery('#group-lot-situation').hide();
    jQuery('#list-lot-container').height(height);
}