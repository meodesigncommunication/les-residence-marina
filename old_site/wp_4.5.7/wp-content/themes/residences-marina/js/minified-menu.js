/**
 * Created by MEO on 11.04.2017.
 */
$(function(){
    $(document).ready(function(){
        $('nav:after').click(function(e){
            toggleNavigation();
        });
    });
    function toggleNavigation()
    {
        $('nav ul').toggle( "fast", function() {
            // Animation complete.
        });
    }
});