<?php
/*
 * Template name: TPL Homepage
 */

$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$context['options'] = wp_load_alloptions();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['title_homepage'] = 'Une qualité de vie<br/>exceptionnelle';
$context['sliders'] = (have_rows('sliders')) ? get_field('sliders') : array();
$context['menus_homepage'] = wp_get_nav_menu_items('home');

Timber::render( 'homepage.twig', $context );