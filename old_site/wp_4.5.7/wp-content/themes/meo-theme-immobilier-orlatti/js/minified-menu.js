/**
 * Created by MEO on 11.04.2017.
 */
$(function(){

    $(document).ready(function(){
        $('nav.minified').click(function(e){
            toggleNavigation();
        });
    });

    $(window).resize(function(){
        minifiedNavigation();
    });

    function toggleNavigation()
    {
        $('nav ul').toggle( "fast", function() {
            // Animation complete.
        });
    }
});