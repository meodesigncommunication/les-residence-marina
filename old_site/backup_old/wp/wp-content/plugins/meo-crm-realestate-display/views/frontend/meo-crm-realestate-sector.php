<?php
/* 
 * Template name: MEO CRM REALESTATE Sector
 */

//Init variables
global $wpdb;
$detect = new Mobile_Detect();
$sector_id = $_GET['id'];
$upload_path = wp_upload_dir();

$wp_navigations = wp_get_nav_menu_items('main');

foreach($wp_navigations as $nav)
{
    if($nav->menu_item_parent != 0)
    {
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['title'] = $nav->title;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['url'] = $nav->url;
        $menus[$nav->menu_item_parent]['childs'][$nav->ID]['object_id'] = $nav->object_id;
    }else {
        $menus[$nav->ID]['title'] = $nav->title;
        $menus[$nav->ID]['url'] = $nav->url;
        $menus[$nav->ID]['object_id'] = $nav->object_id;
    }
}

// Select selected sector
$wheres = array( array('key' => 'id', 'value' => $sector_id, 'before' => '', 'comparator' => '=')); // Array for create a where in query 
$sector = RealestateModel::selectSectorWhere($wheres); // Execute query and return datas

// Select building linked to sector
$wheres = array(array('key' => 'sector_id', 'value' => $sector_id, 'before' => '', 'comparator' => '=')); // Array for create a where in query 
$buildings = RealestateModel::selectBuildingWhere($wheres); // Execute query and return datas

// Init twig variables
$data = Timber::get_context();
$post = new TimberPost();
$data['post'] = $post;
$data['posts'] = Timber::get_posts();
$data['sector'] = $sector[0];
$data['buildings'] = $buildings;
$data['page'] = 'Sector';
$data['charset'] = 'UTF-8';
$data['options'] = wp_load_alloptions();
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['template_path_uri'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['is_mobile'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$data['menus'] = $menus;
$data['current_lang'] = 'fr';
// Show a twig template page

Timber::render('twig/meo-crm-realestate-sector.html.twig', $data);