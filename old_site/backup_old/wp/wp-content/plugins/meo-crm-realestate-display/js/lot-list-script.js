$(document).ready(function(){
    
    executeFilter();
    
    checkFilter();
    
    $('.fancybox').fancybox({
        width: 400,
        height: 500
    });  
    
    $('.btn-situation').fancybox({
        autoDimensions: false,
        autoScale: false,
        showCloseButton:false,
        width: 400
    });

    $('.btn-situation-all-article').fancybox({
        autoDimensions: false,
        autoScale: false,
        showCloseButton:false,
        width: 400
    });
    
    $('.building-name').fancybox({
        autoDimensions: false,
        autoScale: false,
        closeBtn: false,
        showCloseButton:false,
        width: 400
    });
    
    /*
     * Manage Filter for lot list page
     */
    $('#room-filter .filter-item').click(function(){
        if($(this).hasClass('disabled-filter') == false)
        {
            if($(this).hasClass('active') == false)
            {
            $('#room-filter .filter-item.active').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
            var room = $(this).attr('data-value');
            $('input[name="room-selected"]').val(room);
            setSessionFilter('room-selected',room);
            executeFilter();
        }else{
            $(this).removeClass('active');
            $('input[name="room-selected"]').val('');
            setSessionFilter('room-selected','');
            executeFilter();
        }
        }
    });    
    $('#floor-filter .filter-item').click(function(){
        if($(this).hasClass('disabled-filter') == false)
        {
            if($(this).hasClass('active') == false)
            {
                $('#floor-filter .filter-item.active').each(function(){
                    $(this).removeClass('active');
                });
                $(this).addClass('active');
                var floor = $(this).attr('data-value');
                $('input[name="floor-selected"]').val(floor);
                setSessionFilter('floor-selected',floor);
                executeFilter();
            }else{
                $(this).removeClass('active');
                $('input[name="floor-selected"]').val('');
                setSessionFilter('floor-selected','');
                executeFilter();
            }
        }
    });   
    $('#building-filter .filter-item').click(function(){
        if($(this).hasClass('disabled-filter') == false)
        {
            if($(this).hasClass('active') == false)
            {
                $('#building-filter .filter-item.active').each(function(){
                    $(this).removeClass('active');
                });
                $(this).addClass('active');
                var building = $(this).attr('data-value');
                $('input[name="building-selected"]').val(building);
                setSessionFilter('building-selected',building);
                executeFilter();
            }else{
                $(this).removeClass('active');
                $('input[name="building-selected"]').val('');
                setSessionFilter('building-selected','');
                executeFilter();
            }
        }
    });
    $('#surface-filter .filter-item').click(function(){
        if($(this).hasClass('disabled-filter') == false)
        {
            if($(this).hasClass('active') == false)
            {
                $('#surface-filter .filter-item.active').each(function(){
                    $(this).removeClass('active');
                });
                $(this).addClass('active');
                var surface = $(this).attr('data-value');
                $('input[name="surface-selected"]').val(surface);
                setSessionFilter('surface-selected',surface);
                executeFilter();
            }else{
                $(this).removeClass('active');
                $('input[name="surface-selected"]').val('');
                setSessionFilter('surface-selected','');
                executeFilter();
            }
        }
    });  
    /*$('.meta-value-selectable').click(function(){ 
        $(this).parent().find('li').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var meta_value = $(this).attr('data-value'); 
        var input_id = $(this).attr('data-id-input'); 
        $('input[meta-id="' + input_id + '"]').val(meta_value);
        setSessionFilter(input_id,meta_value);
        executeFilter();
        checkFilter();
    });*/
    
    // Manage Button Filter
    $('span.btn-filter').click(function(){
        if($(this).hasClass('view-filter'))
        {
            if(!$(this).hasClass('current'))
            {
                $('#list-filter').slideToggle({
                    duration: 'fast',
                    step: function() {
                        if ($(this).css('display') == 'block') {
                            $(this).css('display', 'table');
                        }
                    },
                    complete: function() {
                        if ($(this).css('display') == 'block') {
                            $(this).css('display', 'table');
                        }
                    }
                });
            }
            $(this).addClass('current');
            $('span.btn-filter.view-all').removeClass('current');                    
        }else{
            if(!$(this).hasClass('current'))
            {
                $('#list-filter').slideToggle({
                    duration: 'fast',
                    step: function() {
                        if ($(this).css('display') == 'block') {
                            $(this).css('display', 'table');
                        }
                    },
                    complete: function() {
                        if ($(this).css('display') == 'block') {
                            $(this).css('display', 'table');
                        }
                    }
                });        
            }
            $(this).addClass('current');
            $('span.btn-filter.view-filter').removeClass('current');
            removeAllFitler();
        }
    });
    
    $('.action-filter span.more_filter').click(function(){
        $('#meta-filter-group').slideToggle( "fast" );
    });
    
    // Remove filter
    $('#remove-filter').click(function(){
        removeAllFitler();
    });
});

function removeAllFitler()
{
    $('input[name="room-selected"]').val('');
    $('input[name="floor-selected"]').val('');
    $('input[name="building-selected"]').val('');
    $('input[name="surface-selected"]').val('');        
    $('.meta-selected').each(function(){
        $(this).val('');
    });
    $('.active').each(function(){
        $(this).removeClass('active');
    });
    deleteSessionFilter();
    executeFilter();
}
function setSessionFilter(filter,value)
{
    $.ajax({
        url  : $('input[name="ajaxurl"]').val(), // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data : ({
            action: 'setSessionFilter',
            index: filter,
            value: value
        }),
        success : function(response){ 
            console.log(response);
        }
    });
}

function deleteSessionFilter()
{
    $.ajax({
        url  : $('input[name="ajaxurl"]').val(), // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data : ({
            action: 'deleteSessionFilter'
        }),
        success : function(response){ 
            console.log('session deleted = '+response);
        }
    });
}

function checkFilter()
{
    var count = 0;
    
    var rooms = [];
    var floors = [];
    var surface = [];
    var buildings = [];
    var metas = [];
    
    $('article.lot-item').each(function(){
        
        console.log($(this).css('display'));
        
        if($(this).css('display') != 'none')
        {           
            rooms[count] = $(this).attr('data-room');
            floors[count] = $(this).attr('data-floor');
            surface[count] = $(this).attr('data-surface');
            buildings[count] = $(this).attr('data-building');            
            count += 1;
        }
    });
    
    $('#room-filter li.filter-item').each(function(){
        var check = false;
        var filter_value = $(this).attr('data-value');
        $.each(rooms, function(key,value){
            if(filter_value == value)
            {
                check = true;
            }
        }); 
        if(check == false)
        {
            $(this).addClass('disabled-filter');
        }else{
            $(this).removeClass('disabled-filter');
        }
    });
    
    $('#floor-filter li.filter-item').each(function(){
        var check = false;
        var filter_value = $(this).attr('data-value');
        
        $.each(floors, function(key,value){
            if(filter_value == value)
            {
                check = true;
            }
        }); 
        if(check == false)
        {
            $(this).addClass('disabled-filter');
        }else{
            $(this).removeClass('disabled-filter');
        }
    });
    
    $('#building-filter li.filter-item').each(function(){
        var check = false;
        var filter_value = $(this).attr('data-value');
        $.each(buildings, function(key,value){
            if(filter_value == value)
            {
                check = true;
            }
        }); 
        if(check == false)
        {
            $(this).addClass('disabled-filter');
        }else{
            $(this).removeClass('disabled-filter');
        }
    });
        
    $('#surface-filter li.filter-item').each(function(){
        var check = false;
        var filter_value = $(this).attr('data-value'); 
        $.each(surface, function(key,value){
            if(filter_value == value)
            {
                check = true;
            }
        }); 
        if(check == false)
        {
            $(this).addClass('disabled-filter');
        }else{
            $(this).removeClass('disabled-filter');
        }
    });
    
}

function executeFilter()
{
    var temp = '';
    var index = '';
    var metas = {};
    var room = $('input[name="room-selected"]').val();
    var floor = $('input[name="floor-selected"]').val();
    var building = $('input[name="building-selected"]').val();
    var surface = $('input[name="surface-selected"]').val();
    
    $('.meta-selected').each(function(){ 
        index = $(this).attr('meta-id');
        if($(this).val() != ''){
            metas['id_'+index] = $(this).val();
        }
    });
    
    $.ajax({
        url  : $('input[name="ajaxurl"]').val(), // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data : ({
            action: 'getListLotFilter',
            room: room,
            floor: floor,
            building: building,
            surface: surface,
            metas: metas
        }),
        success : function(response){ 
            if(response != '')
            {
                $('#message-error').hide();                
                var obj = $.parseJSON(response);
                $('article').hide();  
                $.each(obj, function( index, value ) {
                    $('article[data-id="'+value.id+'"]').show();
                });
            }else{
                $('#message-error').show();
                $('article').hide();
            }
            checkFilter();
        }
    });
}