/**
 * Created by MEO on 11.04.2017.
 */
$(function(){

    $(document).ready(function(){
        minifiedNavigation();
        $('nav.minified').click(function(e){
            toggleNavigation();
        });
    });

    $(window).resize(function(){
        minifiedNavigation();
    });

    function minifiedNavigation()
    {
        var maxWidth = $('nav').width();
        var navWidth = 0;

        $('nav').find('ul').find('li').each(function(){
            navWidth += $(this).width();
            navWidth += parseInt($(this).css('margin-right'));
        });

        if(navWidth > maxWidth){
            $('nav').addClass('minified');
        }else{
            $('nav').removeClass('minified');
        }

        console.log(navWidth+' > '+maxWidth);
    }

    function toggleNavigation()
    {
        $('nav ul').toggle( "fast", function() {
            // Animation complete.
        });
    }
});