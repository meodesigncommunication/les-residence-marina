<?php
/* 
 * Template name: MEO CRM REALESTATE Sector
 */

//Init variables
global $wpdb;
$sector_id = $_GET['id'];
$upload_path = wp_upload_dir();

// Select selected sector
$wheres = array( array('key' => 'id', 'value' => $sector_id, 'before' => '', 'comparator' => '=')); // Array for create a where in query 
$sector = RealestateModel::selectSectorWhere($wheres); // Execute query and return datas

// Select building linked to sector
$wheres = array(array('key' => 'sector_id', 'value' => $sector_id, 'before' => '', 'comparator' => '=')); // Array for create a where in query 
$buildings = RealestateModel::selectBuildingWhere($wheres); // Execute query and return datas


// Init twig variables
$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['sector'] = $sector[0];
$data['buildings'] = $buildings;
$data['page'] = 'Sector';
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

// Show a twig template page
Timber::render('twig/meo-crm-realestate-sector.html.twig', $data);