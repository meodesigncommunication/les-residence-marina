<?php
/* 
 * Template name: MEO CRM REALESTATE Building
 */
$detect = new Mobile_Detect();
$buildings = RealestateModel::selectBuildingsWithLot();
$metas = RealestateModel::selectMeta();
$upload_path = wp_upload_dir();

$data = array();

$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['page'] = 'Building';
$data['plugin_path'] = plugins_url();
$data['type_lot'] = 'Appartements';
$data['metas'] = $metas;
$data['buildings'] = $buildings['buildings'];
$data['building_selected'] = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : 0 ;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

if($detect->isMobile() && !$detect->isTablet())
{
    $data['smartphone'] = true;
}else{
    $data['smartphone'] = false;
}


if($detect->isMobile() || $detect->isTablet())
{
    Timber::render('twig/meo-crm-realestate-building-mobile.html.twig', $data);
}else{
    Timber::render('twig/meo-crm-realestate-building.html.twig', $data);    
}