<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 *  Return listg lot with or without filter
 */
add_action( 'wp_ajax_nopriv_getListLotFilter', 'getListLotFilter' );  
add_action( 'wp_ajax_getListLotFilter', 'getListLotFilter' );
function getListLotFilter()
{
    global $wpdb;
    
    // Get Send Value
    $room = $_POST['room'];
    $floor = $_POST['floor'];
    $surface = $_POST['surface'];
    $metas = $_POST['metas'];
    
    // Execute SQL Query
    $lots_id = RealestateModel::selectListLotFilter($room, $floor, $surface, $metas);
    
    if(count($lots_id) != 0){  
        echo json_encode($lots_id);
    }
    
    die();
}

/*
 *  Create the session filter
 */
add_action( 'wp_ajax_nopriv_setSessionFilter', 'setSessionFilter' );  
add_action( 'wp_ajax_setSessionFilter', 'setSessionFilter' );
function setSessionFilter()
{    
    $index = $_POST['index'];
    $value = $_POST['value'];    
    $_SESSION['filters'][$index] = $value;    
    if($_SESSION['filters'][$index] == $value)
    {
        $check = true;
    }else{
        $check = false;
    }    
    echo $check;    
    die();
}

/*
 *  Delete the session filter
 */
add_action( 'wp_ajax_nopriv_deleteSessionFilter', 'deleteSessionFilter' );  
add_action( 'wp_ajax_deleteSessionFilter', 'deleteSessionFilter' );
function deleteSessionFilter()
{    
    $_SESSION['filters'] = '';    
    if(count($_SESSION['filters']) == 0)    {
        $check = true;
    }else{
        $check = false;
    }    
    echo $check;    
    die();
}
