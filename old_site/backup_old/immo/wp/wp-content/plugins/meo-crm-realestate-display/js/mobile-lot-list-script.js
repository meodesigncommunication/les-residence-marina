$(function(){
    // Open situation popup for mobile device
    $('.mobile-btn-lot').click(function(){
        var bloc_id = $(this).attr('data-id');
        $('#global-content-list-lot').css('display','none');
        $(bloc_id).css('display','block');    
        $(window).scrollTop(0);
    });
    $("#group-lot-situation .lot-content").on("swiperight",function(){
        $(this).css('display','none');
        if($(this).is(':first-child'))
        {
            $("#group-lot-situation .lot-content:last-child").css('display','block');
        }else{
            $(this).prev().css('display','block');
        }
        
    });
    $("#group-lot-situation .lot-content").on("swipeleft",function(){
        $(this).css('display','none');
        if($(this).is(':last-child'))
        {
            $("#group-lot-situation .lot-content:first-child").css('display','block');
        }else{
            $(this).next().css('display','block');
        }
        
    });
    
});

function closeSituation(){
    $('.lot-content').css('display','none');
    $('#global-content-list-lot').css('display','block');
    $(window).scrollTop(0);
}