$(function(){
    
    // Select another building building
    var count = 1;
    var total = $('.building-container').length;
    $('.building-container').each(function(){
        $(this).attr('data-count', count);
        if(count == 1)
        {
            $(this).find('.previous-building-mobile').attr('onclick','changeBuildingBtnMobile('+total+',this)');
            $(this).find('.previous-building-mobile').attr('data-building',total);
            $(this).find('.next-building-mobile').attr('onclick','changeBuildingBtnMobile('+((count*1)+1)+',this)');
            $(this).find('.next-building-mobile').attr('data-building',((count*1)+1));
        }else if(count == total)
        {
            $(this).find('.previous-building-mobile').attr('onclick','changeBuildingBtnMobile('+((count*1)-1)+',this)');
            $(this).find('.previous-building-mobile').attr('data-building',((count*1)-1));
            $(this).find('.next-building-mobile').attr('onclick','changeBuildingBtnMobile(1,this)');
            $(this).find('.next-building-mobile').attr('data-building','1');
        }else{
            $(this).find('.previous-building-mobile').attr('onclick','changeBuildingBtnMobile('+((count*1)-1)+',this)');
            $(this).find('.previous-building-mobile').attr('data-building',((count*1)-1));
            $(this).find('.next-building-mobile').attr('onclick','changeBuildingBtnMobile('+((count*1)+1)+',this)');
            $(this).find('.next-building-mobile').attr('data-building',((count*1)+1));
        }
        count++;
    });
    
    $('.details_building').click(function(){
        if($(this).parents('.building-details').find('.info-building').css('display') == 'none')
        {
            $(this).parents('.building-details').find('.info-building').slideDown(1000);
            $(this).find('.fa-chevron-right').addClass('hidden');
            $(this).find('.fa-chevron-down').removeClass('hidden');
        }else{
            $(this).parents('.building-details').find('.info-building').slideUp(500);
            $(this).find('.fa-chevron-down').addClass('hidden');
            $(this).find('.fa-chevron-right').removeClass('hidden');
        }
    });
    
    /* DETAILS LOT ON MOBILE VERSION */
    /*$('.btn-show-lot-details').click(function(){
        var id_show = $(this).attr('id-show');
        $(this).parents('.building-container').find('#popup-vignette').show();
        $(this).parents('.building-container').find('#popup-vignette').find('#details-lot-'+id_show).show();
        $(this).parents('.building-container').find('.building-content').hide();
        $(this).parents('.building-container').find('.table-lots').hide();
    });*/
    
    $('.btn-close-lot-details').click(function(){
        $(this).parents('#popup-vignette').hide();
        $(this).parents('.fancybox-details-lot').hide();
        $(this).parents('.building-container').find('.building-content').show();
        $(this).parents('.building-container').find('.table-lots').show();
    });
});

function show_lot_plane(element)
{
    var id_show = $(element).attr('id-show');
    $(element).parents('.building-container').find('#popup-vignette').show();
    $(element).parents('.building-container').find('#popup-vignette').find('#details-lot-'+id_show).show();
    $(element).parents('.building-container').find('.building-content').hide();
    $(element).parents('.building-container').find('.table-lots').hide();
}