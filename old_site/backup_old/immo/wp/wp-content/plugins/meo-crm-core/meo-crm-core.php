<?php

/*
Plugin Name: MEO CRM Core
Description: Core Plugin MEO CRM site
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

define('MEO_CORE_PLUGIN_SLUG', 		'meo-crm-core');
define('GOOGLE_CORE_CSS', 			'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
define('GOOGLE_CORE_JS', 			'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
define('GOOGLE_CORE_CLIENT_ID', 	'378538563046-fdge1tn1otpg8km2rijhe1034ik5n0d6.apps.googleusercontent.com');
define('MEO_CORE_MAIL_NAME', 		'MEO Real Estate');
define('MEO_CORE_MAIL_USER_FROM', 	'no-replys@meo-realestate.com');
define('MEO_CORE_MAIL_USER_TO', 	'digital@meomeo.ch');

# Librairies
require_once( $plugin_root . "librairies/fpdf/fpdf.php");
require_once( $plugin_root . 'librairies/fpdf/fpdi.php');

# Classes
require_once( $plugin_root . 'classes/meo-crm-core-tools.php');
require_once( $plugin_root . 'classes/meo-crm-core-templater.php');
require_once( $plugin_root . 'classes/meo-crm-core-crypt-data.php');
require_once( $plugin_root . 'classes/meo-crm-core-mobile-detect.php');
require_once( $plugin_root . 'classes/meo-crm-core-shortcode.php');
require_once( $plugin_root . 'classes/meo-crm-core-locator.php');
$MeoCrmCoreLocator = new MeoCrmCoreLocator();
$MeoCrmCoreShortcode = new MeoCrmCoreShortcode();

add_shortcode( 'meo_crm_core_previous_page' , array( MeoCrmCoreHelper, 'getButtonPreviousPage' ) );


# Tools
add_action( 'activated_plugin', 'meo_crm_core_load_first' );
// TODO::Maybe add also when plugins loaded
function meo_crm_core_load_first() {
	$path = str_replace( WP_PLUGIN_DIR . '/', '', __FILE__ );
	if ( $plugins = get_option( 'active_plugins' ) ) {
		if ( $key = array_search( $path, $plugins ) ) {
			array_splice( $plugins, $key, 1 );
			array_unshift( $plugins, $path );
			update_option( 'active_plugins', $plugins );
		}
	}
}

function meo_crm_core_slug_exists($slug) {
	global $wpdb;
	if($wpdb->get_row("SELECT post_name FROM ".$wpdb->prefix."posts WHERE post_name = '" . $slug . "'", 'ARRAY_A')) {
		return true;
	} else {
		return false;
	}
}

function meo_crm_core_is_mobile() {
	$detect = new Mobile_Detect;
	return $detect->isMobile();
}

function meo_crm_core_is_tablet() {
	$detect = new Mobile_Detect;
	return $detect->isTablet();
}

add_action('init', 'meo_crm_core_StartSession', 1);

function meo_crm_core_StartSession() {
	if(!session_id()) {
		session_start();
	}
}

# Add Scripts and Styles

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script_style_core' );
function load_custom_wp_admin_script_style_core() {

        # JS
    	wp_register_script( 'meo_crm_core_helper_list_js',  '/wp-content/plugins/meo-crm-core/js/meo-crm-core-helper-list.js', false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_core_helper_list_js' );
        
        wp_register_script( 'threesixty-spinner-jquery',  '/wp-content/plugins/meo-crm-core/js/threesixty-spinner-jquery.js', false, '1.0.0', true );
        wp_enqueue_script( 'threesixty-spinner-jquery' );
        
        wp_register_script( 'meo_crm_core_google_js',  GOOGLE_CORE_JS, false, '1.0.0' );
        wp_enqueue_script( 'meo_crm_core_google_js' );
        
        # CSS
        wp_register_style( 'meo_crm_core_google_css',  GOOGLE_CORE_CSS, false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_core_google_css' );

        # CSS
        wp_register_style( 'meo_crm_core_helper_list_css',  '/wp-content/plugins/meo-crm-core/css/meo-crm-core-helper-list.css', false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_core_helper_list_css' );

}

/*
 *  Load Files For Fancybox Plugin
 */
add_action( 'wp_enqueue_scripts', 'load_custom_wp_script_style_core' );
function load_custom_wp_script_style_core() {

	# JS
	wp_register_script('meo_crm_core_mousewheel_script','/wp-content/plugins/meo-crm-core/librairies/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_mousewheel_script' );

	wp_register_script('meo_crm_core_fancybox_script','/wp-content/plugins/meo-crm-core/librairies/fancybox/source/jquery.fancybox.pack.js',false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_script' );

	wp_register_script('meo_crm_core_fancybox_buttons_script','/wp-content/plugins/meo-crm-core/librairies/fancybox/source/helpers/jquery.fancybox-buttons.js',false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_buttons_script' );

	wp_register_script('meo_crm_core_fancybox_thumbs_script','/wp-content/plugins/meo-crm-core/librairies/fancybox/source/helpers/jquery.fancybox-thumbs.js',false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_thumbs_script' );

	wp_register_script('meo_crm_core_fancybox_media_script','/wp-content/plugins/meo-crm-core/librairies/fancybox/source/helpers/jquery.fancybox-media.js',false,'1.0.0');
	wp_enqueue_script( 'meo_crm_core_fancybox_media_script' );

	# CSS
	wp_register_style( 'meo_crm_core_fancybox_css',  '/wp-content/plugins/meo-crm-core/librairies/fancybox/source/jquery.fancybox.css', false, '1.0.0');
	wp_enqueue_style( 'meo_crm_core_fancybox_css' );

	wp_register_style( 'meo_crm_core_fancybox_button_css',  '/wp-content/plugins/meo-crm-core/librairies/fancybox/source/helpers/jquery.fancybox-buttons.css', false, '1.0.0');
	wp_enqueue_style( 'meo_crm_core_fancybox_button_css' );

	wp_register_style( 'meo_crm_core_fancybox_thumbs_css',  '/wp-content/plugins/meo-crm-core/librairies/fancybox/source/helpers/jquery.fancybox-thumbs.css', false, '1.0.0');
	wp_enqueue_style( 'meo_crm_core_fancybox_thumbs_css' );


	// Enqueue Dashicons style for frontend use
	wp_enqueue_style( 'dashicons' );
}

# PAGES & TEMPLATES
	
	# Register Plugins Pages
	add_action( 'plugins_meo_crm_active_create_page', 'meo_crm_core_pages_register' );
	
	function meo_crm_core_pages_register() {
		
		# Call for plugins pages
		$pluginsPages = array();
		$pluginsPages = apply_filters('meo_crm_core_front_pages_collector', $pluginsPages);
			
		# Create Pages for Plugins
		if(is_array($pluginsPages)){
			
			foreach($pluginsPages as $pluginSlug => $pluginPage){
				
				if(!meo_crm_core_slug_exists($pluginSlug)){
										
					$exports_page = array(
							'post_title' => $pluginPage['post_title'],
							'post_content' => $pluginPage['post_content'],
							'post_status' => 'publish',
							'post_type' => 'page',
							'post_author' => 1,
							'post_date' => date('Y-m-d H:i:s'),
							'post_name' => $pluginSlug,
							'meta_input' => array('_wp_page_template' => $pluginPage['_wp_page_template'])
					);
				
					wp_insert_post($exports_page);
				}
				
			}
			
		}
				
			
	}
	
	// TODO::ADD Core admin page to store wp_option values for core mechanism