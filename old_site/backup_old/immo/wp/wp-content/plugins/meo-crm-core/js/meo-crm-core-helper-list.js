/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.$ = jQuery;

$(document).ready(function(){
    /*
     *  Gestion des checkbox (All checked)
     */
    $('input.cb-select-all[type=checkbox]').click(function(){
       if($(this).is(':checked'))
       {
           $('input.cb-select-all[type=checkbox]').attr('checked', true);
           $('.meo-crm-list-checkbox').each(function(){
               $(this).attr('checked', true);
           });
       }else{
           $('input.cb-select-all[type=checkbox]').attr('checked', false);
           $('.meo-crm-list-checkbox').each(function(){
               $(this).attr('checked', false);
           });
       }
    }); 
});

/*
 *  Gestion des delete par ligne
 */
function trash(id,action,text)
{   
    if(text == '')
    {
        text = 'Voulez-vous supprimer cette élément ?';
    }
    if(confirm(text))
    {
        $.post(
           ajaxurl,
           {
               'action': action,
               'id': id
           },
           function(response){
                console.log(response);
                var obj = $.parseJSON(response);
                console.log(obj);
                $('div#zoneMessage').html(obj.message);
                $('#content-table').html(obj.table);
            }
        );
    }
}