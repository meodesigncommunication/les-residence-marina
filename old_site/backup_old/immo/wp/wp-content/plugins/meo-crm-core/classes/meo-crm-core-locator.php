<?php 
/*
 * MeoCrmCoreLocator Class
 * 
 * 
 * Shortcode to set in Wordpress [meo_crm_core_countrieslist lang="fr" class="class"]
 * Shortcode to set in CF7 form : [meo_crm_core_countrieslist_cf7 lang:fr class:custom_class]
 * Shortcode to set in CF7 mail [meo_crm_core_country]
 */
Class MeoCrmCoreLocator {
	
	const DEFAULTLANG = 'fr';
	
	private static $countries;
	private static $defaultCountry;
	
	public function __construct($lang = DEFAULTLANG){
	
		# Wordpress Add Country shortcode : [meo_crm_core_countrieslist lang="fr" class="class"]
		add_shortcode( 'meo_crm_core_countrieslist' , array( MeoCrmCoreLocator, 'generateCountriesListHTMLDropDown' ) );
		
		# CF7 Add country custom shortcode : [meo_crm_core_countrieslist_cf7]
		add_action( 'wpcf7_init', array( MeoCrmCoreLocator, 'setCF7countryShortcode' ) );
		
		# CF7 Mail Country shortcode interception
		add_filter( 'wpcf7_special_mail_tags', array( MeoCrmCoreLocator, 'setCF7MailShortcode' ), 10, 3 );
		
		# CF7 Intercept posted data
		add_filter( 'wpcf7_posted_data', array( MeoCrmCoreLocator, 'setCF7PostedData' ), 1, 1);
		
		# Meo Crm Contacts / Analytics filter
		add_filter( 'meo_crm_contact_data', array( MeoCrmCoreLocator, 'getGoogleCoordinates' ));
		 
	}
	
	
	############################
	# Generation And Execution #
	############################
	
	
		# Generate HTML countries dropdown select	
		public function generateCountriesListHTMLDropDown($atts){
			
			$atts = shortcode_atts( array(
					'lang' => DEFAULTLANG,
					'class' => '',
			), $atts );
		
		
			$countriesList = self::getCountriesList($atts['lang']);
			
			$defaultCountrySelected = self::getDefaultCountry($atts['lang']);
			
			$html = '<span class="wpcf7-form-control-wrap country">
						<select id="country" name="country" required="required" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required '.$atts['class'].'" aria-invalid="false" aria-required="true">';
			
			$defaultCode = key($defaultCountrySelected);
			
			$html.= '<option selected="selected" value="'.$defaultCode.'">'.$defaultCountrySelected[$defaultCode].'</option>';
			$html.= '<option value="">'.__('Choisissez votre pays').'</option>';
			
			foreach($countriesList as $continent => $list){
				
				$html.= '<optgroup label="'.$continent.'">';
				
				if(is_array($list)){
					
					foreach($list as $code => $countryName){
						$html.= '<option value="'.$code.'">'.$countryName.'</option>';
					}
				}
				
				$html.= '</optgroup>';
				
			}
			
			$html.= '</select>
				</span>';
			
			return $html;
		}
		
		# Set CF7 country shortcode to implement into form
		public function setCF7countryShortcode() {
			wpcf7_add_shortcode( 'meo_crm_core_countrieslist_cf7', array( MeoCrmCoreLocator, 'setCF7countryShortcodeHandler') );
		}
		
		# Set Action when interpreting the defined shortcode
		public function setCF7countryShortcodeHandler( $tag ) {
			
			$shortcodeAtts = '';
			
			# Expected options are writen as key:value
			if(is_array($tag['options']) && count($tag['options']) > 0){
				foreach($tag['options'] as $index => $optionPair){
					$explodePair = explode(':', $optionPair);
					
					$shortcodeAtts.= ' '.$explodePair[0].'="'.$explodePair[1].'"';
				}
			}
			
			return do_shortcode('[meo_crm_core_countrieslist'.$shortcodeAtts.']');
		}
	
	
	##############
	# Get values #
	##############
	
	
		# Default Country
		static public function getDefaultCountry($lang = DEFAULTLANG){
			return self::setDefaultCountry($lang);
		}
		
		# Countries List
		static public function getCountriesList($lang = DEFAULTLANG){
			return self::setCountriesLists($lang);
		}
		
		# Corresponding country name
		static public function getCountryNameByCode( $countryCode, $lang ) {
		
			$countriesList = self::getCountriesList($lang);
		
			foreach($countriesList as $continent => $list){
		
				if(is_array($list)){
		
					foreach($list as $code => $countryName){
						if($code == $countryCode) return esc_html($countryName);
					}
				}
		
			}
		
			return false;
		}
		
		# Google call to get longitude and latitude based on contact address
		static public function getGoogleCoordinates($dataContact){
			
			if(		array_key_exists('address', $dataContact) && array_key_exists('postcode', $dataContact) 
				 && array_key_exists('city', $dataContact) && array_key_exists('country', $dataContact)){
				
				$tempAddress = $dataContact['address'].', '.$dataContact['postcode'].', '.$dataContact['city'].', '.$dataContact['country'];
				$builtAddress = str_replace(" ", "+", $tempAddress);
					
				# CURL Call
				$curl = new WP_Http_Curl();
				
				$response = $curl->request("https://maps.googleapis.com/maps/api/geocode/json?address=$builtAddress&sensor=false", array(
						'headers' => array(
								'Accept'       => 'application/json',
								'Content-type' => 'application/json',
						),
						'method' => 'GET',
						'redirection' => 0
				));
					
				if(is_array($response) && array_key_exists('body', $response)){
					$dataGoogleMaps = json_decode($response['body']);
					$dataGoogleMapsArray = reset($dataGoogleMaps);
					$coordinates = reset($dataGoogleMapsArray);
					$dataContact['latitude'] = $coordinates->geometry->location->lat;
					$dataContact['longitude'] = $coordinates->geometry->location->lng;
				}
			}				
			
			return $dataContact;
		}
		
	
	
	##############
	# Set values #
	##############
		
		
		# Default Country
		private function setDefaultCountry($lang = DEFAULTLANG){
		
		
			self::$defaultCountry = array(
		
					'fr' => array("CH" => "Suisse"),
					'en' => array("CH" => "Switzerland"),
			);
			
			return self::$defaultCountry[$lang];
		
		}
		
		# Countries List
		private function setCountriesLists($lang = DEFAULTLANG){
			
			
			self::$countries = array(
					
				'fr' => array(
						"Europe" => array (
								"AL" => "Albanie", 					"DE" => "Allemagne",						"AD" => "Andorre",					"AN" => "Antilles N&eacute;erlandaises",
								"AT" => "Autriche",					"BY" => "Belarus",							"BE" => "Belgique",					"BA" => "Bosnie-Herz&eacute;govine",
								"BG" => "Bulgarie",					"CY" => "Chypre",							"HR" => "Croatie",					"CW" => "Curacao",
								"DK" => "Danemark",					"ES" => "Espagne",							"EE" => "Estonie",					"FO" => "Fero&euml; (Iles)",
								"FI" => "Finlande",					"FR" => "France",							"GI" => "Gibraltar",				"GD" => "Grenade",
								"GL" => "Gro&euml;nland",			"GR" => "Gr&ecirc;ce",						"GP" => "Guadeloupe",				"GG" => "Guernesey",
								"GF" => "Guyane Francaise",			"HU" => "Hongrie",							"IE" => "Irlande",					"IS" => "Islande",
								"IT" => "Italie",					"JE" => "Jersey (Ile)",						"LV" => "Lettonie",					"LI" => "Liechtenstein",
								"LT" => "Lituanie",					"LU" => "Luxembourg",						"MK" => "Mac&eacute;doine",			"MT" => "Malte",
								"MQ" => "Martinique",				"MD" => "Moldovie",							"MC" => "Monaco",					"ME" => "Mont&eacute;n&eacute;gro",
								"NO" => "Norv&egrave;ge",			"NC" => "Nouvelle Cal&eacute;donie",		"NL" => "Pays-Bas",					"BQ" => "Pays-Bas carib&eacute;ens",
								"PL" => "Pologne",					"PF" => "Polynesie Francaise",				"PT" => "Portugal",					"RO" => "Roumanie",
								"GB" => "Royaume-Uni",				"RU" => "Russie",							"RE" => "R&eacute;union",			"PM" => "Saint Pierre et Miquelon",
								"SM" => "Saint-Marin",				"MF" => "Saint-Martin",						"VA" => "Saint-Si&egrave;ge (&Eacute;tat de la cit&eacute; du Vatican)",
								"VC" => "Saint-Vincent-et-Grenadines",											"RS" => "Serbie et Montenegro",		"SK" => "Slovaque (Rep.)",
								"SI" => "Slov&eacute;nie",			"SE" => "Su&egrave;de",						"CH" => "Suisse",					"SJ" => "Svalbard et &Icirc;le Jan Mayen",
								"CZ" => "Tch&egrave;que (Rep.)",	"TF" => "Terres Australes Francaise",		"UA" => "Ukraine",					"BV" => "&Icirc;le Bouvet",
								"IM" => "&Icirc;le de Man",			"AX" => "&Icirc;les &Acirc;land",
						),
						"Am&eacute;riques" => array (
								"AI" => "Anguilla",
								"AG" => "Antigua et Barbuda",		"AR" => "Argentine",						"AW" => "Aruba",					"BS" => "Bahamas",
								"BB" => "Barbade",					"BZ" => "Belize",							"BM" => "Bermudes",					"BO" => "Bolivie",
								"BR" => "Br&eacute;sil",			"CA" => "Canada",							"KY" => "Cayman (Iles)",			"CL" => "Chili",
								"CO" => "Colombie",					"CR" => "Costa Rica",						"CU" => "Cuba",						"DO" => "Dominicaine (Rep.)",
								"DM" => "Dominique",				"SV" => "El Salvador",						"EC" => "Equateur",					"US" => "Etats-Unis d\'Amerique",
								"FK" => "Falkland (Iles)",			"GU" => "Guam",								"GT" => "Guatemala",				"GY" => "Guyana",
								"GS" => "G&eacute;orgie du Sud et les &Icirc;les Sandwich du Sud",				"HT" => "Ha&iuml;ti",				"HN" => "Honduras",
								"JM" => "Jama&iuml;que",			"MX" => "Mexique",							"MS" => "Montserrat",				"NI" => "Nicaragua",
								"PA" => "Panama",					"PY" => "Paraguay",							"PN" => "Pitcairn",					"PR" => "Porto Rico",
								"PE" => "P&eacute;rou",				"BL" => "Saint-Barth&eacute;lemy",			"KN" => "Saint-Kitts-et-Nevis",		"SX" => "Saint-Martin (Partie N&eacute;erlandaise)",
								"SH" => "Sainte-H&eacute;l&egrave;ne",											"LC" => "Sainte-Lucie",				"AS" => "Samoa Am&eacute;ricaines",
								"ST" => "Sao Tome-et-Principe",		"SL" => "Sierra Leone",						"TT" => "Trinit&eacute;-et-Tobago",	"TC" => "Turks et Ca&iuml;cos (Iles)",
								"UY" => "Uruguay",					"VE" => "Venezuela",						"VI" => "Vierges Am&eacute;ricaines (Iles)",
								"VG" => "Vierges Britanniques (Iles)",
						),
						"Asie/Oc&eacute;anie" => array(
								"AF" => "Afghanistan",				"AQ" => "Antarctique",						"SA" => "Arabie Saoudite",			"AM" => "Arm&eacute;nie",
								"AC" => "Ascension",				"HM" => "Australia Heard Island and McDonald Islands",
								"AU" => "Australie",				"AZ" => "Azerbaidjan",						"BH" => "Bahrein",					"BD" => "Bangladesh",
								"BT" => "Bhoutan",					"BN" => "Brunei",							"KH" => "Cambodge",					"CN" => "Chine",
								"CX" => "Christmas Island",			"CK" => "Cook (Iles)",						"KR" => "Cor&eacute;e (Rep. de)",	"KP" => "Cor&eacute;e (Rep. Dem. Pop.)",
								"AE" => "Emirats Arabes Unis",		"FJ" => "Fidji",							"GE" => "G&eacute;orgie",			"HK" => "Hong-Kong",
								"IN" => "Inde",						"ID" => "Indon&eacute;sie",					"IQ" => "Irak",						"IR" => "Iran (Rep. Isl.)",
								"CC" => "Islands Cocos (Keeling) Islands",										"IL" => "Isra&euml;l",				"JP" => "Japon",
								"JO" => "Jordanie",					"KZ" => "Kazakhstan",						"KG" => "Kirghizistan",				"KI" => "Kiribati",
								"KW" => "Kowe&iuml;t",				"LA" => "Laos (Rep. Dem. Pop. du)",			"LB" => "Liban",					"MO" => "Macao",
								"MY" => "Malaisie",					"MV" => "Maldives",							"MP" => "Mariannes (Iles)",			"MH" => "Marshall (Iles)",
								"FM" => "Micron&eacute;sie",		"MN" => "Mongolie",							"MM" => "Myanmar",					"NR" => "Nauru",
								"NU" => "Niue",						"NF" => "Norfolk (Ile)",					"NZ" => "Nouvelle-Z&eacute;lande",	"NP" => "N&eacute;pal",
								"OM" => "Oman",						"UZ" => "Ouzbekistan",						"PK" => "Pakistan",					"PW" => "Palau",
								"PS" => "Palestine",				"PG" => "Papouasie-Nouvelle-Guin&eacute;e",	"PH" => "Philippines",				"QA" => "Qatar",
								"SB" => "Salomon",					"WS" => "Samoa-Occidental",					"SC" => "Seychelles",				"SG" => "Singapour",
								"LK" => "Sri Lanka",				"SR" => "Surinam",							"SY" => "Syrie",					"TJ" => "Tadjikistan (Rep. du)",
								"TW" => "Ta&iuml;wan",				"IO" => "Territoire britannique de l\'oc&eacute;an Indien",
								"TH" => "Tha&iuml;lande",			"TP" => "Timor Oriental",					"TL" => "Timor-Leste",				"TK" => "Tokelau",
								"TO" => "Tonga",					"TM" => "Turkmenistan",						"TR" => "Turquie",					"TV" => "Tuvalu",
								"VU" => "Vanuatu",					"VN" => "Vietnam",							"WF" => "Wallis et Futuna",			"YE" => "Y&eacute;men (Rep.)",
						),
						"Afrique" => array(
								"ZA" => "Afrique du Sud",			"DZ" => "Alg&eacute;rie",					"AO" => "Angola",					"BW" => "Botswana",
								"BF" => "Burkina Faso",				"BI" => "Burundi",							"BJ" => "B&eacute;nin",				"CM" => "Cameroun",
								"CV" => "Cap-Vert",					"CF" => "Centrafricaine (Rep.)",			"KM" => "Comores",					"CG" => "Congo",
								"CD" => "Congo Za&iuml;re (Rep. Dem.)",											"CI" => "C&ocirc;te d\'Ivoire",		"DJ" => "Djibouti",
								"EG" => "Egypte",					"ZZ" => "Equatorial Kundu",					"ER" => "Erythr&eacute;e",			"ET" => "Ethiopie",
								"GA" => "Gabon",					"GM" => "Gambie",							"GH" => "Ghana",					"GN" => "Guin&eacute;e",
								"GQ" => "Guin&eacute;e Equatoriale","GW" => "Guin&eacute;e-Bissau",				"KE" => "Kenya",					"LS" => "Lesotho",
								"LR" => "Liberia",					"LY" => "Libye",							"MG" => "Madagascar",				"MW" => "Malawi",
								"ML" => "Mali",						"MA" => "Maroc",							"MU" => "Maurice",					"MR" => "Mauritanie",
								"YT" => "Mayotte",					"MZ" => "Mozambique",						"NA" => "Namibie",					"NE" => "Niger",
								"NG" => "Nigeria",					"UG" => "Ouganda",							"RW" => "Rwanda",					"EH" => "Sahara Occidental",
								"SO" => "Somalie",					"SD" => "Soudan",							"SS" => "Soudan du Sud",			"SZ" => "Swaziland",
								"SN" => "S&eacute;n&eacute;gal",	"TZ" => "Tanzanie",							"TD" => "Tchad",					"TG" => "Togo",
								"TN" => "Tunisie",					"ZM" => "Zambie",							"ZW" => "Zimbabwe",
						)
				),
	
					
				'en' => array(
						
						"Europe" => array(
								"AL" => "Albania",					"AD" => "Andorra",							"AT" => "Austria",					"BY" => "Belarus",
								"BE" => "Belgium",					"BA" => "Bosnia and Herzegovina",			"BG" => "Bulgaria",					"HR" => "Croatia",
								"CY" => "Cyprus",					"CZ" => "Czech Republic",					"DK" => "Denmark",					"DD" => "East Germany",
								"EE" => "Estonia",					"FO" => "Faroe Islands",					"FI" => "Finland",					"FR" => "France",
								"DE" => "Germany",					"GI" => "Gibraltar",						"GR" => "Greece",					"GG" => "Guernsey",
								"HU" => "Hungary",					"IS" => "Iceland",							"IE" => "Ireland",					"IM" => "Isle of Man",
								"IT" => "Italy",					"JE" => "Jersey",							"LV" => "Latvia",					"LI" => "Liechtenstein",
								"LT" => "Lithuania",				"LU" => "Luxembourg",						"MK" => "Macedonia",				"MT" => "Malta",
								"FX" => "Metropolitan France",		"MD" => "Moldova",							"MC" => "Monaco",					"ME" => "Montenegro",
								"NL" => "Netherlands",				"NO" => "Norway",							"PL" => "Poland",					"PT" => "Portugal",
								"RO" => "Romania",					"RU" => "Russia",							"SM" => "San Marino",				"RS" => "Serbia",
								"CS" => "Serbia and Montenegro",	"SK" => "Slovakia",							"SI" => "Slovenia",					"ES" => "Spain",
								"SJ" => "Svalbard and Jan Mayen",	"SE" => "Sweden",							"CH" => "Switzerland",				"UA" => "Ukraine",
								"SU" => "Union of Soviet Socialist Republics",									"GB" => "United Kingdom",			"VA" => "Vatican City",
								"AX" => "&Aring;land Islands",
						),
	
						"Americas" => array(
								"AI" => "Anguilla",					"AG" => "Antigua and Barbuda",				"AR" => "Argentina",				"AW" => "Aruba",
								"BS" => "Bahamas",					"BB" => "Barbados",							"BZ" => "Belize",					"BM" => "Bermuda",
								"BO" => "Bolivia",					"BR" => "Brazil",							"VG" => "British Virgin Islands",	"CA" => "Canada",
								"KY" => "Cayman Islands",			"CL" => "Chile",							"CO" => "Colombia",					"CR" => "Costa Rica",
								"CU" => "Cuba",						"DM" => "Dominica",							"DO" => "Dominican Republic",		"EC" => "Ecuador",
								"SV" => "El Salvador",				"FK" => "Falkland Islands",					"GF" => "French Guiana",			"GL" => "Greenland",
								"GD" => "Grenada",					"GP" => "Guadeloupe",						"GT" => "Guatemala",				"GY" => "Guyana",
								"HT" => "Haiti",					"HN" => "Honduras",							"JM" => "Jamaica",					"MQ" => "Martinique",
								"MX" => "Mexico",					"MS" => "Montserrat",						"AN" => "Netherlands Antilles",		"NI" => "Nicaragua",
								"PA" => "Panama",					"PY" => "Paraguay",							"PE" => "Peru",						"PR" => "Puerto Rico",
								"BL" => "Saint Barth&eacute;lemy",	"KN" => "Saint Kitts and Nevis",			"LC" => "Saint Lucia",				"MF" => "Saint Martin",
								"PM" => "Saint Pierre and Miquelon","VC" => "Saint Vincent and the Grenadines",	"SR" => "Suriname",					"TT" => "Trinidad and Tobago",
								"TC" => "Turks and Caicos Islands",	"VI" => "U.S. Virgin Islands",				"US" => "United States",			"UY" => "Uruguay",
								"VE" => "Venezuela",
						),
						"Asia/Oceania" => array(
								"AF" => "Afghanistan",				"AS" => "American Samoa",					"AQ" => "Antarctica",				"AM" => "Armenia",
								"AU" => "Australia",				"AZ" => "Azerbaijan",						"BH" => "Bahrain",					"BD" => "Bangladesh",
								"BT" => "Bhutan",					"BV" => "Bouvet Island",					"IO" => "British Indian Ocean Territory",
								"BN" => "Brunei",					"KH" => "Cambodia",							"CN" => "China",					"CX" => "Christmas Island",
								"CC" => "Cocos [Keeling] Islands",	"CK" => "Cook Islands",						"CY" => "Cyprus",					"FJ" => "Fiji",
								"PF" => "French Polynesia",			"TF" => "French Southern Territories",		"GE" => "Georgia",					"GU" => "Guam",
								"HK" => "Hong Kong SAR China",		"HM" => "Heard Island and McDonald Islands","IN" => "India",					"ID" => "Indonesia",
								"IR" => "Iran",						"IQ" => "Iraq",								"IL" => "Israel",					"JP" => "Japan",
								"JO" => "Jordan",					"KZ" => "Kazakhstan",						"KI" => "Kiribati",					"KW" => "Kuwait",
								"KG" => "Kyrgyzstan",				"LA" => "Laos",								"LB" => "Lebanon",					"MO" => "Macau SAR China",
								"MY" => "Malaysia",					"MV" => "Maldives",							"MH" => "Marshall Islands",			"FM" => "Micronesia",
								"MN" => "Mongolia",					"MM" => "Myanmar [Burma]",					"NR" => "Nauru",					"NP" => "Nepal",
								"NT" => "Neutral Zone",				"NC" => "New Caledonia",					"NZ" => "New Zealand",				"NU" => "Niue",
								"NF" => "Norfolk Island",			"KP" => "North Korea",						"MP" => "Northern Mariana Islands",	"OM" => "Oman",
								"PK" => "Pakistan",					"PW" => "Palau",							"PS" => "Palestinian Territories",	"PG" => "Papua New Guinea",
								"YD" => "People's Democratic Republic of Yemen",								"PH" => "Philippines",				"PN" => "Pitcairn Islands",
								"QA" => "Qatar",					"WS" => "Samoa",							"SA" => "Saudi Arabia",				"SG" => "Singapore",
								"SB" => "Solomon Islands",			"GS" => "South Georgia and the South Sandwich Islands",
								"KR" => "South Korea",				"LK" => "Sri Lanka",						"SY" => "Syria",					"TW" => "Taiwan",
								"TJ" => "Tajikistan",				"TH" => "Thailand",							"TL" => "Timor-Leste",				"TK" => "Tokelau",
								"TO" => "Tonga",					"TR" => "Turkey",							"TM" => "Turkmenistan",				"TV" => "Tuvalu",
								"UM" => "U.S. Minor Outlying Islands",											"AE" => "United Arab Emirates",		"UZ" => "Uzbekistan",
								"VU" => "Vanuatu",					"VN" => "Vietnam",							"YE" => "Yemen",					"WF" => "Wallis and Futuna",
						),
						"Africa" => array(
								"DZ" => "Algeria",					"AO" => "Angola",							"BJ" => "Benin",					"BW" => "Botswana",
								"BF" => "Burkina Faso",				"BI" => "Burundi",							"CM" => "Cameroon",					"CV" => "Cape Verde",
								"CF" => "Central African Republic",	"TD" => "Chad",								"KM" => "Comoros",					"CG" => "Congo - Brazzaville",
								"CD" => "Congo - Kinshasa",			"CI" => "C&ocirc;te d'Ivoire",				"DJ" => "Djibouti",					"EG" => "Egypt",
								"GQ" => "Equatorial Guinea",		"ER" => "Eritrea",							"ET" => "Ethiopia",					"GA" => "Gabon",
								"GM" => "Gambia",					"GH" => "Ghana",							"GN" => "Guinea",					"GW" => "Guinea-Bissau",
								"KE" => "Kenya",					"LS" => "Lesotho",							"LR" => "Liberia",					"LY" => "Libya",
								"MG" => "Madagascar",				"MW" => "Malawi",							"ML" => "Mali",						"MR" => "Mauritania",
								"MU" => "Mauritius",				"YT" => "Mayotte",							"MA" => "Morocco",					"MZ" => "Mozambique",
								"NA" => "Namibia",					"NE" => "Niger",							"NG" => "Nigeria",					"RW" => "Rwanda",
								"RE" => "R&eacute;union",			"SH" => "Saint Helena",						"SN" => "Senegal",					"SC" => "Seychelles",
								"SL" => "Sierra Leone",				"SO" => "Somalia",							"ZA" => "South Africa",				"SD" => "Sudan",
								"SZ" => "Swaziland",				"ST" => "S&atilde;o Tom&eacute; and Pr&iacute;ncipe",
								"TZ" => "Tanzania",					"TG" => "Togo",								"TN" => "Tunisia",					"UG" => "Uganda",
								"EH" => "Western Sahara",			"ZM" => "Zambia",							"ZW" => "Zimbabwe",
						),
				),
					
					
			);
			
			return self::$countries[$lang];
		}
	
		# Set CF7 Posted Data, mainly country: change country code to country name
		static public function setCF7PostedData($postedData){
			
			if(is_array($postedData) && array_key_exists('country', $postedData)){
				
				$lang = DEFAULTLANG;
				if(array_key_exists('language', $postedData)) $lang = $postedData['language'];
				
				$postedData['country'] = self::getCountryNameByCode($postedData['country'], $lang);
			}
			
			return $postedData;
		}
		

		# Set CF7 mail shortcode value for meo_crm_core_country
		static public function setCF7MailShortcode( $output, $name, $html ) {
			
			$lang = DEFAULTLANG;
			if(array_key_exists('language', $_POST)) $lang = $_POST['language'];
			
			if ( $name == 'meo_crm_core_country' && array_key_exists('country', $_POST) )
				$output = self::getCountryNameByCode($_POST['country'], $lang);
		
			return $output;
		}
	
}
?>