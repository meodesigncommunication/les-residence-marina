<?php

/*
 * 
 */
class MeoCrmCoreShortcode
{
    
    public function __construct(){ 
        add_action( 'wpcf7_init', array( MeoCrmCoreShortcode, 'setCF7PreviousPageShortcode' ) );
    }
    
    # Set CF7 country shortcode to implement into form
    public function setCF7PreviousPageShortcode() {
        wpcf7_add_shortcode( 'meo_crm_core_previous_page_cf7', array( MeoCrmCoreShortcode, 'setCF7PreviousPageShortcodeHandler') );
    }

    # Set Action when interpreting the defined shortcode
    public function setCF7PreviousPageShortcodeHandler() {
        $detect = new Mobile_Detect();
        if($detect->isMobile() && !$detect->isTablet())
        {
            $url = $_SERVER['HTTP_REFERER'];        
            return '<a id="previous-page-button" href="'.$url.'" title="back to previous page">Back</a>';
        }else{
            return '';
        }
    }
    
}

