<?php
# Defines / Constants
define('MESSAGE_REQUIRED', 'Le champ @replace est obligatoire');
define('MESSAGE_VALID_ARRAY', 'Le champ @replace a rencontrer un probl&egrav;me');
define('MESSAGE_MIN', 'Le champ @replace est trop court');
define('MESSAGE_MAX', 'Le champ @replace est trop long');
define('MESSAGE_EMAIL', 'Le champ @replace doit &ecirc;tre un email');
define('MESSAGE_DATE', 'Le champ @replace doit &ecirc;tre une date');
define('MESSAGE_ALPHA', 'Le champ @replace ne doit contenir que du texte');
define('MESSAGE_NUMERIC', 'Le champ @replace ne doit contenir que des nombres');
define('MESSAGE_ALPHANUMERIC', 'Le champ @replace ne doit contenir que des nombres et du texte');
define('MESSAGE_TEXTE', 'Le champ @replace contient des caractère non permis');
define('MESSAGE_URL', 'Le champ @replace doit &ecirc;tre un URL');
define('MESSAGE_SLUG', 'Le champ @replace doit &ecirc;tre un SLUG');


/*
 *  Class for check a validity of data in form
 */
class MeoCrmCoreValidationForm
{
    
    public function validationDatas($rules,$post)
    {
        $result = array();
        foreach($rules as $key => $rule)
        {
            $data = array(
                'key' => $key,
                'value' => $post[$key]
            );
            
            $rules_defined = explode('|', $rule);
            
            foreach($rules_defined as $rule_defined)
            {                
                $rules_param = explode(':', $rule_defined);
                $result[] = $this->validateProcess($data, $rules_param[0], $rules_param[1]);
            }
        }
        return $result;
    }
    
    public function checkResultValidation($results)
    {
        $ckeck = true;
        $message = '';
        
        foreach($results AS $result)
        {
            if(!$result['valid'])
            {
                $ckeck = false;
                $message .= '<strong>ERREUR : </strong>'.$result['message'].'<br/>';
            }
        }
        
        return array(            
            'result' => $ckeck,
            'message' => $message
        );
    }
    
    private function validateProcess($data,$type,$param=null)
    {
        switch($type)
        {
            case 'required':
                $result = $this->required($data);
                break;

            case 'email':
                $result = $this->email($data);
                break;

            case 'date':
                $result = $this->date($data);
                break;

            case 'alpha':
                $result = $this->alpha($data);
                break;
            
            case 'float':
                $result = $this->float($data);
                break;

            case 'array':
                $result = $this->valideArray($data);
                break;

            case 'numeric':
                $result = $this->numeric($data);
                break;

            case 'alphaNumeric':
                $result = $this->alphaNumeric($data);
                break;

            case 'text':
                $result = $this->text($data);
                break;

            case 'url':
                $result = $this->url($data);
                break;
            
            case 'slug':
                $result = $this->slug($data);
                break;

            case 'max':
                $result = $this->max($data,$param);
                break;

            case 'min':
                $result = $this->min($data,$param);
                break;
        } 
        
        return $result;
    }
    
    private function required($data)
    {
        $result = true;
        $message = '';
        
        if(!isset($data['value']) || empty($data['value']))
        {
            $result = false;
            $message = str_replace('@replace', $data['key'], MESSAGE_REQUIRED);
        } 
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        ); 
    }
    
    private function valideArray($data)
    {
        $result = true;
        $message = '';
        
        if(!is_array($data) || empty($data))
        {
            $result = false;
            $message = str_replace('@replace', $data['key'], MESSAGE_VALID_ARRAY);
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        ); 
    }
    
    private function min($data,$param=0)
    {
        $result = true;
        $message = '';
        
        if(strlen($data['value']) < $param)
        {
            $result = false;
            $message = str_replace('@replace', $data['key'], MESSAGE_MIN);
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        ); 
    }
    
    private function max($data,$param=0)
    {
        $result = true;
        $message = '';
        
        if(strlen($data['value']) > $param)
        {
            $result = false;
            $message = str_replace('@replace', $data['key'], MESSAGE_MAX);
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        ); 
    }
    
    private function executeRegex($regex,$value)
    {
        if(preg_match('/^'.$regex.'$/', $value))
        {
            return true;
        }
        return false;
    }
    
    private function email($data)
    {
        $regex = '(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_EMAIL);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );    
    }
    private function date($data)
    {
        $regex = '(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_DATE);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function alpha($data)
    {
        $regex = '[a-z][A-Z]*';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_ALPHA);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function numeric($data)
    {
        $regex = '[0-9]*';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_NUMERIC);
        }else{
            $result = true;
        }
       
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function float($data)
    {
        $data['value'] = str_replace(',', '.', $data['value']);
        $regex = '[-+]?[0-9]*[.]?[0-9]+';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_NUMERIC);
        }else{
            $result = true;
        }
       
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function alphaNumeric($data)
    {
        $regex = '[A-Za-z\d_]*';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_ALPHANUMERIC);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function text($data)
    {
        $regex = '[ A-Za-z0-9_@ÀÁÂÃÄÅÇÑñÇçÈÉÊËÌÍÎÏÒÓÔÕÖØÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ.,;:\/#&+\-!?()\s\'"]*';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_TEXTE);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function url($data)
    {
        $regex = '((http:\/\/|https:\/\/)?(www.)?(([a-zA-Z0-9-]){2,}\.){1,4}([a-zA-Z]){2,6}(\/([a-zA-Z-_\/\.0-9:?=&;,]*)?)?)';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_URL);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
    
    private function slug($data)
    {
        $regex = '[a-z0-9\-_]+';
        $message = '';
        $result = $this->executeRegex($regex,$data['value']);
        
        if(!$result && !empty($data['value']))
        {
            $message = str_replace('@replace', $data['key'], MESSAGE_SLUG);
        }else{
            $result = true;
        }
        
        return array(
            'key' => $data['key'],
            'valid' => $result,
            'message' => $message
        );  
    }
}