<?php
/*
 * MEO CRM CORE - TOOLS CLASS
 * 
 * Contains several functions to be used across the site
 * 
 */

class MeoCrmCoreTools {
		
	# Check email addresses string, and validate each of them
    public static function meo_crm_core_emailStringToArray($emailString, $separator = ',') {
        	
		$raw_emails = explode($separator, $emailString);
			
		$emails = array();
			
		foreach ($raw_emails as $raw_email) {
			# Validation
			if (is_email($raw_email)) {
				$emails[] = $raw_email;
			}
		}
		return $emails;
	}
	
	# Errors Handling
	public static function meo_crm_core_report_error($file, $line, $message) {
		
		if (!defined('MEO_CORE_MAIL_USER_TO') || MEO_CORE_MAIL_USER_TO == '' ) {
			return '';
		}
		
		$content = "Error in ".$file.", line ".$line.":\n\n" . print_r($message, true);
		$headers = array( 'From: '.MEO_CORE_MAIL_NAME.' Error <' . MEO_CORE_MAIL_USER_FROM . '>' );
		
		wp_mail( MEO_CORE_MAIL_USER_TO, 'MEO Real Estate :: Error', $content, $headers );
	}

} 

?>
