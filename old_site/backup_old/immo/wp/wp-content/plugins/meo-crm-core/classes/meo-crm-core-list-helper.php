<?php

/**
 *  Class qui permet de generer des tables (liste de donnees)
 */
class MeoCrmCoreListHelper
{
    private $key_id;
    private $header;
    private $datas;
    private $selected_list;
    private $action_list;
    private $action;
    
    public function __construct() 
    {
        
    }
   
    /*
     * Methode qui va créer toute la table
     * 
     * @param $datas(array), $header(array), $list_action(array), $select_list(boolean), $key_id(string)
     * @return $html(string)
     */
    public function getList($datas, $header, $list_action=array(), $select_list=false,$key_id='id')
    {
        // Init variables
        $this->key_id = $key_id;
        $this->header = $header;
        $this->datas = $datas;
        $this->selected_list = $select_list;
        $this->action_list = $list_action;
        
        // Check si le tableau a des action
        if(is_array($list_action) && !empty($list_action))
        {
            $this->action = true;
        }else{
            $this->action = false;
        }
        
        $html  = '<table class="wp-list-table widefat fixed striped posts">';
        $html .=    $this->getListHeader();
        if(is_array($datas) && !empty($datas))
        {
            $html .= $this->getListContent();
        }else{
            $html .= $this->getListContentEmpty();
        }        
        $html .= '</table>';
        
        return $html;
        
    }
    
    /*
     * create thead and tfoot of the array
     * 
     * @return $html(string)
     */
    private function getListHeader()
    {
        $html  = '<thead>';
        $html .=    $this->generateHeader();
        $html .= '</thead>';
        $html .= '<tfoot>';
        $html .=    $this->generateHeader();
        $html .= '</tfoot>';
        
        return $html;        
    }
    
    /*
     * Create content of header or footer of array
     * 
     * @return $html(string)
     */
    private function generateHeader()
    {
        $html  = '<tr>';
        
        if($this->selected_list)
        {
            $html .= '<th class="w30px center-text"><input type="checkbox" class="cb-select-all" name="cb-select-all" /></th>';
        }
        
        foreach($this->header as $header)
        {
            $html .= '<th class="'.$header['class'].'">'.$header['name'].'</th>';
        }
        
        if($this->action)
        {
            $html .= '<th>Actions</th>';
        }
        
        $html .= '</tr>';
        
        return $html;
    }
    
    /*
     * Generate a content of array if datas isn't empty
     * 
     * @return $html(string)
     */
    private function getListContent()
    {
        $html  = '<tbody>';
        foreach($this->datas as $data)
        {
            $html .= '<tr>';
            $html .= ($this->selected_list) ? '<td class="w30px center-text"><input id="list_'.$data[$this->key_id].'" class="meo-crm-list-checkbox" type="checkbox" value="'.$data[$this->key_id].'" name="list[]"></td>' : '';
            foreach($this->header as $header)
            {
                $html .= $this->getListSpecialCell($data[$header['key']],$header['type_data'],$header['class']);
            }            
            $html .= ($this->action) ? $this->getActionCell($data) : '';
            $html .= '</tr>';
        }
        $html .= '</tbody>';
        return $html;
    }
   
    /*
     * Generate a content of array if datas is empty
     * 
     * @return $html(string)
     */
    private function getListContentEmpty()
    {
        $html  = '<tbody>';
        $html .= '<tr>';
        $html .= ($this->selected_list) ? '<td class="w30px center-text"></td>' : '';
        $html .= '<td colspan="'.count($this->header).'">Aucun résultas</td>';       
        $html .= ($this->action) ? '<td></td>' : '';
        $html .= '</tr>';
        $html .= '</tbody>';
        return $html;
    }
    
    /*
     * Generate a content of cell 
     * 
     * @return $html(string)
     */
    private function getListSpecialCell($value,$type='base',$class)
    {
        switch($type)
        {
            case 'base':
                $html = '<td class="'.$class.'">'.$value.'</td>';
                break;
            
            case 'boolean':
                $content = ($value)? '<i class="fa fa-check green"></i>' : '<i class="fa fa-close red"></i>' ;
                $html = '<td class="'.$class.'">'.$content.'</td>';
                break;
            
            case 'image':
                $content = ($value)? '<i class="fa fa-check green"></i>' : '<i class="fa fa-close red"></i>' ;
                $html = '<td class="'.$class.'"><img width="150" src="'.$content.'" alt=""/></td>';
                break;
        }
        
        return $html;
    }
    
    /*
     * Generate action of array if is not empty
     * 
     * @return $html(string)
     */
    private function getActionCell($data)
    {
        $html  = '<td>';
        foreach($this->action_list as $action)
        {
            $search = (isset($action['search']) && !empty($action['search'])) ? $action['search'] : '';
            $replace = (isset($action['replace']) && !empty($action['replace'])) ? $data[$action['replace']] : '';
                    
            if(isset($action['url_action']) && !empty($action['url_action']))
            {
                $url_action = str_replace($search, $replace, $action['url_action']);
            }
            
            if(isset($action['click_action']) && !empty($action['click_action']))
            {
                $click_action = str_replace($search, $replace, $action['click_action']);
            }
            
            $add_html = (isset($action['url_action']) && !empty($action['url_action'])) ? 'href="'.$url_action.'"' : 'onclick="'.$click_action.'"' ;
            $html .= '<a '.$add_html.'  class="'.$action['class'].'" ><i class="'.$action['icon'].'"></i></a>';
        }
        $html .= '</td>';
        return $html;
    }
}