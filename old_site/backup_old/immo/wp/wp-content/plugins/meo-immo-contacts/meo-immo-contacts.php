<?php

/*
Plugin Name: MEO IMMO Contacts
Description: Plugin to collect and transfert Contacts data
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );


# Defines / Constants
define('MEO_CONTACTS_PLUGIN_ROOT', 				$plugin_root);
define('MEO_CONTACTS_PLUGIN_SLUG', 				'meo-immo-contacts');
define('MEO_CONTACTS_SLUG', 					'meo-immo-contacts');					// Contacts page slug (Frontend)

# Required Files
// require_once( $plugin_root . 'class-meo-immo-contacts.php');


# Globals

/*
 * Check for Dependencies :: MEO CRM Core
 */
function meo_immo_contacts_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-core/meo-crm-core.php' ) ) {
		$installed_dependencies = true;
	}

	if(!$installed_dependencies) {

		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Core plugin before', 'meo-realestate').'</h3></div>';

		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Core plugin before.', 'meo-realestate'), E_USER_ERROR);
		exit;


	}
	else {

		// Everything is fine

		global $wpdb;
	}

}
register_activation_hook(__FILE__, 'meo_immo_contacts_activate');


# Add Ajax calls and functions

	# Update Contact Field
	
	add_action( 'wp_ajax_get_contact', 'meo_immo_contacts_getAjaxContact' );
	add_action( 'wp_ajax_nopriv_get_contact', 'meo_immo_contacts_getAjaxContact' );
	
		function meo_immo_contacts_getAjaxContact() {
			global $wpdb;
		
			
			header('Content-Type: application/json');
			
			// $this->checkApiKey();
			
			$analytics_id = $_REQUEST['analytics_id'];
			
			$result = array();
			
			if (isset($analytics_id) && !empty($analytics_id)) {
				$analytics_id = urldecode($analytics_id);
				$analytics_id = preg_split('/,/', $analytics_id);
				$analytics_ids = array();
				foreach ($analytics_id as $analytics_id_element) {
					$analytics_ids[] = preg_replace('/[^a-z0-9A-Z]/', '', $analytics_id_element);
				}
				// $result = array('id' => $analytics_id);
				//$entries = $this->getDownloads($analytics_ids);
				
				

				// $lots = mred_get_lots();
				$lots = array();
				$sql = "SELECT @row_number := if ( @last_submit_time = entries.submit_time, @row_number, @row_number + 1) as row_number,
						entries.form_name, entries.field_name, entries.field_value, entries.field_order, entries.submit_time,
						@last_submit_time := entries.submit_time as last_submit_time
							FROM {$wpdb->prefix}cf7dbplugin_submits entries
								join {$wpdb->prefix}cf7dbplugin_submits withanalytics on entries.submit_time = withanalytics.submit_time and withanalytics.field_name = 'analytics_id'
								join (select @last_submit_time := 0.0) a
								join (select @row_number := 0) b
									where entries.field_name not in ('Submitted Login', 'Submitted From', 'pdf-download')" .
									(empty($analytics_ids) ? '' : ' and withanalytics.field_value in ("' . join('","', $analytics_ids) . '")' ) .
									" order by entries.submit_time, entries.field_order";
		
				$raw_downloads = $wpdb->get_results($sql);
		
				$entries = array();
		
				foreach ($raw_downloads as $index => &$raw_download) {
					
					unset($raw_downloads[$index]->last_submit_time);
					
					if (!isset($entries[$raw_download->row_number]['contact_type'])) {
						$entries[$raw_download->row_number]['contact_type'] = 'contact';
					}
					
					$entries[$raw_download->row_number]['form_name'] = $raw_download->form_name;
					$entries[$raw_download->row_number]['submit_time'] = $raw_download->submit_time;
					
					$field_name = ( $raw_download->field_name == "sender" ? "email" : $raw_download->field_name );
					
					$entries[$raw_download->row_number][$field_name] = $raw_download->field_value;
					
					if ($field_name == "file_id" && !empty($meosc)) {
						
						$file_id = $meosc->decodeAttachmentId((int) $raw_download->field_value);
						
						$entries[$raw_download->row_number][$field_name] = $file_id;
						$entries[$raw_download->row_number]['contact_type'] = 'file';
						
						unset($file_id);
					}
					else if ($field_name == "post_id") {
						
						if (!empty($lots[$raw_download->field_value])) {
							$entries[$raw_download->row_number]["lot_code"] = $lots[$raw_download->field_value]['name'];
						}
						else {
							$post_details = get_post($raw_download->field_value);
							$entries[$raw_download->row_number]["lot_code"] = __($post_details->post_title);
						}
					}
					
					unset($raw_downloads[$index]);
				}
		
				
				foreach($entries as $entry) {
					$entry['submit_time'] = date_i18n('d F Y G:i:s', floatval($entry['submit_time']));
					$result[] = $entry;
				}
			}
			echo json_encode($result);
			exit;
			
			
			
			
			wp_die();
		}