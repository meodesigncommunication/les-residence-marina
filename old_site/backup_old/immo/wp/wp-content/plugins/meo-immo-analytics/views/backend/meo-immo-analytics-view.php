<?php 
function meo_immo_analytics_admin_page(){ 
	
	global $immoOptions;
	
	// Populate data
	$data = array();
	
	foreach ($immoOptions as $immoOption => $optionDetail){
		$data[$immoOption] = get_option($immoOption);
	}
	
	// Form Validation
	if (isset($_POST['submit_immo_analytics'])){
		
		foreach ($immoOptions as $immoOption => $optionDetail){
			update_option($immoOption, $_POST[$immoOption]);
			// Over ride populated data
			$data[$immoOption] = $_POST[$immoOption];
		}
	}
?>

<form id="" name="" action="" method="post">
					
<div>
	<table class="form-table meo-form-table">
		<tr class="form-field">
			<td colspan="2"><h2>Smart Capture config &amp; PDF email details</h2></td>
		</tr>
		<tr class="form-field">
			<th><label>Piwir MEO Server</label></th>
			<td><em><?php echo MEO_ANALYTICS_SERVER; ?></em></td>
		</tr>
		<?php 
		// Prepare options
		foreach ($immoOptions as $immoOption => $optionDetail){
			echo '<tr class="form-field">
					<th><label>'.$optionDetail['label'].'</label></th>
					<td>';
					if($optionDetail['type'] == 'textarea'){
						wp_editor( $data[$immoOption], str_replace('_', '-', $immoOption), array('textarea_name' => $immoOption, 'textarea_rows' => 10, 'media_buttons' => false, 'teeny' => true) );
					}
					else echo '<input name="'.$immoOption.'" type="'.$optionDetail['type'].'" value="'.$data[$immoOption].'" />';
					
					if($optionDetail['notes'] != ''){
						echo '<br/><em>Notes: '.$optionDetail['notes'].'</em>';
					}
					
			echo '</td>
				</tr>';
		}
		?>
		<tr class="form-field">
			<td colspan="2"><input type="submit" name="submit_immo_analytics" value="save" /></td>
		</tr>
	</table>
</div>					
					
</form>					
1. Install and activate the latest versions of:
   - CF7 plugin (http://wordpress.org/plugins/contact-form-7/) - required
   - ACF plugin (https://wordpress.org/plugins/advanced-custom-fields/) - required
   - ACF Options Page plugin (http://www.advancedcustomfields.com/add-ons/options-page/) - required
   - Contact Form DB plugin (http://wordpress.org/plugins/contact-form-7-to-database-extension/) - required
   - Listo plugin (http://wordpress.org/plugins/listo/) - optional (for lists of eg countries)
   - Easy Fancybox (http://wordpress.org/plugins/easy-fancybox/)


3. Create a new form in CF7 (one per language), eg
	Form:
		<p><label for="surname">Nom*<br /></label>[text* surname placeholder "Nom*"]</p>
		<p><label for="first_name">Pr&eacute;nom*<br /></label>[text* first_name placeholder "Pr&eacute;nom*"]</p>
		<p><label for="email">Email*<br /></label>[email* email placeholder "Email*"]</p>
		<p><label for="phone">T&eacute;l*<br /></label>[text* phone placeholder "T&eacute;l*"]</p>
		<p><label for="address">Adresse*<br /></label>[text* address placeholder "Adresse*"]</p>
		<p><label for="postcode">NPA*<br /></label>[text* postcode placeholder "NPA*"]</p>
		<p><label for="city">Lieu*<br /></label>[text* city placeholder "Lieu*"]</p>
		<p><label for="country">Pays*<br /></label>[select* country data:countries.olympic]</p>
		[file_id* file_id]<input type="hidden" name="pdf-download" value="1">* champs obligatoires<br>
		[response]<br><br>
		<p>[submit "Envoyer"]</p>
		<div class="clear"></div>
		<input type="hidden" name="language" value="fr">

	From: [surname], [first_name] <[email]>

	Subject: Development :: PDF commandé

	Body:
		Nom: [surname]
		Pr&eacute;nom: [first_name]
		email: [email]
		T&eacute;lephone: [phone]
		Adresse: [address]
		NPA: [postcode]
		Lieu: [city]
		Pays: [country]
		Langue: fr

		Item: [lot_code]

4. Create a page (called eg "File request")
	Content: Only the shortcode for the contact form
	Template: MSC Attachment Request
	Complete the "PDF Download" section


6. For lightboxes:
	Install and activate Easy FancyBox plugin
	In Settings .. Media, under FancyBox .. Media, select "iframes" (only) and save
	Then set width and height (for pixels, no unit; for percentage xx% format)
	Ensure any links have a class of fancybox-iframe.  The dimensions can be overriden on individual links with (eg) class="fancybox-iframe {width:600,height:500}"
<?php 
}
?>