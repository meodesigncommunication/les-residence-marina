<?php
/**
 * Template Name: MSC Attachment Download Handler
 */

function msc_adh_show_error($message) {
	get_header(); ?>

	<div class="content-wrapper">
		<div id="content">
			<div id="content-inner">
				<h2><?php _e('Error', MSC_TEXT_DOMAIN); ?></h2>
				<div class="error"><?php echo $message; ?></div>
			</div>
		</div>
	</div>

	<?php get_footer();
	exit;
}


global $wpdb, $meoMIMEtypes;

$email = urldecode($_GET['email']);
$download_code = urldecode($_GET['download_code']);

$query = $wpdb->prepare("SELECT file.id, file.attachment_id, person.language
							FROM ".$wpdb->prefix.MEO_ANALYTICS_FILEDL_TABLE." file
							JOIN ".$wpdb->prefix.MEO_ANALYTICS_FILEDLDER_TABLE." person ON file.file_downloader_id = person.id
								WHERE person.email = %s
								AND file.download_code = %s", array($email, $download_code));

SELECT file.id, file.attachment_id, person.language FROM wp_meo_immo_file_download file JOIN wp_meo_immo_file_downloader person ON file.file_downloader_id = person.id WHERE person.email = 'frederic.ansermoz@meomeo.ch' AND file.download_code = '96582841416024068'
echo $query;
$file_details = $wpdb->get_row($query);

if (empty($file_details)) {
	msc_adh_show_error(__('Invalid email address or download code', MSC_TEXT_DOMAIN));
}

$post_details = get_post( $file_details->attachment_id );

if (empty($post_details) or !in_array($post_details->post_mime_type, $meoMIMEtypes)) {
	msc_adh_show_error(__('Missing or invalid plan details for lot', MSC_TEXT_DOMAIN));
}

// Update the counter
$wpdb->query( $wpdb->prepare("UPDATE ".$wpdb->prefix.MEO_ANALYTICS_FILEDL_TABLE." 
								SET download_count = download_count + 1 
								WHERE id = %d", (int) $file_details->id ) );

$file = get_attached_file($file_details->attachment_id);
$lang_file = preg_replace('/pdf$/', $file_details->language . '.pdf', $file);
if (file_exists($lang_file)) {
	$file = $lang_file;
}

$filename = basename($file);

header('Content-type: ' . $post_details->post_mime_type);
header('Content-Disposition: attachment; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');

@readfile($file);
