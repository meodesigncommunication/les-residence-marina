<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoScCf7Integration {

	private $smartcapture = null;
	private $utilities = null;


	// Build the instance
	public function __construct($smartcapture, $utilities) {
		$this->smartcapture = $smartcapture;
		$this->utilities = $utilities;
	}

	// Prepare hooks, filters, actions and shortcodes
	public function addHooks() {
		add_action( 'init', array( $this, 'mscWpcf7AddShortcode' ) );
		add_action( 'init', array( $this, 'mscCf7dbAddHooks' ) );
	}

	// Add shortcodes / filters / actions
	public function mscWpcf7AddShortcode() {
		
		// Should always exist, as there's an explicit dependency between the plugins
		if (function_exists('wpcf7_add_shortcode')) {
			
			// Add fields
			/*wpcf7_add_shortcode('file_id',  array($this, 'mscCf7FileIdShortcode' ), true);
			wpcf7_add_shortcode('file_id*', array($this, 'mscCf7FileIdShortcode' ), true);

			wpcf7_add_shortcode('post_id',  array($this, 'mscCf7PostIdShortcode' ), true);
			wpcf7_add_shortcode('post_id*', array($this, 'mscCf7PostIdShortcode' ), true);*/
			
			wpcf7_add_shortcode('file_id',  array($this, 'mscCf7FieldShortcode' ), true);
			wpcf7_add_shortcode('file_id*', array($this, 'mscCf7FieldShortcode' ), true);
			
			wpcf7_add_shortcode('post_id',  array($this, 'mscCf7FieldShortcode' ), true);
			wpcf7_add_shortcode('post_id*', array($this, 'mscCf7FieldShortcode' ), true);
			
			wpcf7_add_shortcode('inputPreviousUrl',  		array($this, 'mscCf7inputPreviousUrlShortcode' ), true);
			wpcf7_add_shortcode('buttonCancelPreviousUrl',  array($this, 'mscCf7buttonCancelPreviousUrlShortcode' ), true);

			// Add fields validation
			add_filter('wpcf7_validate_file_id',  array($this, 'mscCf7ValidateFileId'), 10, 2);
			add_filter('wpcf7_validate_file_id*', array($this, 'mscCf7ValidateFileId'), 10, 2);

			// Countries list to be sorted
			add_filter( 'wpcf7_form_tag_data_option', array($this, 'mscCf7SortListoCountries'), 99, 3 );

			// Handle file request
			add_action('wpcf7_before_send_mail', array($this, 'mscHandleFileRequest'));
		}
	}

	// Hooks specific to the CF7 to DB plugin
	public function mscCf7dbAddHooks() {
		
		// Make every effort to get the analytics ID
		add_filter( 'cfdb_form_data', array($this, 'mscCf7dbSetAnalyticsId'), 1);
	}
	/*
	// File id input field shortcode
	public function mscCf7FileIdShortcode($tag) {
		if (!is_array($tag)) {
			return '';
		}

		$name = $tag['name'];
		if (empty($name)) {
			return '';
		}

		$file_id = (int) $_GET['file_id'];

		return '<input type="hidden" name="' . $name . '" value="' . $file_id . '" />';
	}

	public function mscCf7PostIdShortcode($tag) {
		if (!is_array($tag)) {
			return '';
		}

		$name = $tag['name'];
		if (empty($name)) {
			return '';
		}

		$post_id = (int) $_GET['post_id'];

		return '<input type="hidden" name="' . $name . '" value="' . $post_id . '" />';
	}
	*/
	// Try only one function
	public function mscCf7FieldShortcode($tag) {
		if (!is_array($tag)) {
			return '';
		}
	
		$name = $tag['name'];
		if (empty($name)) {
			return '';
		}
	
		$value = (int) $_GET[$name];
	
		return '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
	}
	
	# TODO :: Recheck the two functions below (from estavayer theme)
	
	/* Shortcode to generate input hidden [inputPreviousUrl url ]
	 * [inputPreviousUrl url=""] */
	
	public function mscCf7inputPreviousUrlShortcode() {
		$url = '';
		if(isset($_GET['current_url']) && !empty($_GET['current_url']))
		{
			$url = 'http://'.$_GET['current_url'];
		}
		return '<input type="hidden" name="pervious_url" value="'.$url.'">';
	}
	
	/* Shortcode to generate button cancel for file request page on mobile device [buttonCancelPreviousUrl url ]
	 * [buttonCancelPreviousUrl url=""] */
	
	public function mscCf7buttonCancelPreviousUrlShortcode() {
	
		$url = '';
		$html = '';
	
		if(isset($_GET['current_url']) && !empty($_GET['current_url']))
		{
			$url = 'http://'.$_GET['current_url'];
			$html = '<a class="wpcf7-button-cancel" href="'.$url.'"><input type="button" value="Annuler" /></a>';
		}
	
		return $html;
	}
	
	
	
	
	

	// Validate file_id field
	public function mscCf7ValidateFileId($result, $tag) {
		$type = $tag['type'];
		$name = $tag['name'];

		$obfuscated_id = (int) $_POST[$name];
		$file_id = $this->utilities->decodeAttachmentId($obfuscated_id);

		$file = get_post($file_id);

		// test if not use $meoMIMEtypes instead of only pdf
		if ( (empty($file) || $file->post_mime_type != 'application/pdf') && ($type == 'file_id*' || !empty($_POST[$name]))){
			$result['valid'] = false;
			$result['reason'][$name] = wpcf7_get_message( 'invalid_required' );
		}

		return $result;
	}

	public function mscCf7SortListoCountries( $data, $options, $args ) {
		if (!is_array($data)) {
			return $data;
		}

		$leaders = array(
			'che', // Switzerland
			'fra', // France
			'deu', // Germany
			'ita', // Italy
			'gbr'  // UK
		);

		array_multisort(array_map(array($this->utilities, 'mscCf7AccentedSort'), $data), $data);

		// Move specific countries to the top of the list
		$leader_countries = array();
		foreach ($leaders as $leader) {
			$leader_countries[$leader] = $data[$leader];
		}

		$data = $leader_countries + $data;
		return $data;
	}

	/*
	 * Prepare Mailing or not, based on form submission. Your download email and rest
	 */
	public function mscHandleFileRequest($cf7) {
		
		if (!is_a($cf7, 'WPCF7_ContactForm')) {
			return;
		}

		$submission = WPCF7_Submission::get_instance();
		if (!$submission) {
			return;
		}

		$posted_data = $submission->get_posted_data();

		if (!isset($posted_data['file_id']) or !$posted_data['file_id']
			or !isset($posted_data['pdf-download']) or !$posted_data['pdf-download']) {
			return;
		}

		// Get the id and decode it
		$obfuscated_id = (int) $posted_data['file_id'];
		$file_id = $this->utilities->decodeAttachmentId($obfuscated_id);

		// Lot ID is optional
		$lot_id = (int) $_GET['post_id'];
		$lot = get_post($lot_id);
		if (!empty($lot)) {
			// Set lot code in email sent to developer
			$mail = $cf7->prop( 'mail' );
			$mail['body'] = str_replace('[lot_code]',__($lot->post_title), $mail['body']);
			$cf7->set_properties( array( 'mail' => $mail ) );
		}

		// Starting to build the email
		$download_url = $this->smartcapture->getDownloadLink($cf7, $posted_data, $file_id);
		
		$email_sender      = get_option('email_sender');
		$email_sender_name = get_option('email_sender_name');
		$email_bcc         = get_option('email_bcc');
		$logo              = get_option('logo');
		
		list($email_subject, $email_content) = $this->getEmailContent($file_id, $lot, $posted_data);

		if (empty($email_content)) {
			return;
		}

		$logo_tag = '';
		if (!empty($logo) && !empty($logo['url'])) {
			$logo_tag = '<img src="' . $logo['url'] . '" alt="' . $logo['title'] . '" />';
		}


		$replacements = array(
								'[download_url]'   => $download_url,
								'[lot_code]'       => __($lot->post_title),
								'[logo]'           => $logo_tag,
								'[sender_email]'   => $email_sender
							);

		foreach ($replacements as $replacement_code => $replacement_value) {
			$email_subject = str_replace($replacement_code, $replacement_value, $email_subject);
			$email_content = str_replace($replacement_code, $replacement_value, $email_content);
		}

		foreach ($posted_data as $posted => $data) {
			$email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		}
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		// $email_content .= '<br/>'.$posted.' : '.$data.'<br/>';
		
		$headers = array(
			'From: ' . $email_sender_name . ' <' . $email_sender . '>',
			'Content-type: text/html; charset="utf-8"'
		);

		if (!empty($email_bcc)) {
			$headers[] = 'Bcc: ' . $email_bcc;
		}

		// Send email
		wp_mail($posted_data['email'], $email_subject, $email_content, $headers);
		
		// Update : if everything went fine, we set a Session value to print prices over the front end (only if price list file has been downloaded)
		$this->mscCheckDownloadedItem($file_id);
	}

	// Function to verify if the downloaded media is the price list one
	public function mscCheckDownloadedItem($decodedMediaID){
	
		// Check if it's a price enabler
		if(get_post_meta($decodedMediaID, "_price_enabler", true)) { 
			
			// Enable prices on the frontend for the current session
			$_SESSION['pricesEnabled'] = true;
			return true;
		}
		else return false;
	}

	private function getEmailContent($file_id, $lot, $posted_data) {
		// email content may be in one of three places
		// 1. the attachment itself
		// 2. the post's taxonomy eg lot type
		// 3. the post containing the form (most general - usually a fallback)
		$email_subject = get_option('email_subject');
		$email_content = get_option('email_content');

		if (!empty($email_content)) {
			return array($email_subject, $email_content);
		}

		if (!empty($lot)) {
			$taxonomies =  get_taxonomies();
			foreach ($taxonomies as $taxonomy) {
				$terms = wp_get_post_terms( $lot->ID, $taxonomy);
				if (empty($terms)) {
					continue;
				}
				foreach ($terms as $term) {
					$email_subject = get_option('email_subject');
					$email_content = get_option('email_content');
					if (!empty($email_content)) {
						return array($email_subject, $email_content);
					}
				}
			}
		}


		// Get the ID of the page the form was on.  Matches logic in wpcf7_special_mail_tag_for_post_data()
		if ( ! preg_match( '/^wpcf7-f(\d+)-p(\d+)-o(\d+)$/', $posted_data['_wpcf7_unit_tag'], $matches ) ) {
			return array(null, null);
		}
		$post_id = (int) $matches[2];

		$email_subject = get_option('email_subject');
		$email_content = get_option('email_content');

		return array($email_subject, $email_content);
	}

	public function mscCf7dbSetAnalyticsId($cf7) {

		if (isset($cf7->posted_data['analytics_id'])) {
			return $cf7;
		}

		require_once( dirname(__FILE__) . '/class-meosc-piwik-integration.php');
		$analytics_id = MeoPiwikIntegration::mscPiwikGetAnalyticsIdFromCookie();

		if (!empty($analytics_id)) {
			$cf7->posted_data['analytics_id'] = $analytics_id;
		}

		return $cf7;
	}
}
