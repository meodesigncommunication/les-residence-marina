<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoErrorHandler {

	// TODO: add admin screen
	const REPORT_ERRORS_TO = 'digital@meomeo.ch';

	public static function reportError($file, $line, $message) {

		if (empty(self::REPORT_ERRORS_TO) ) {
			return;
		}
		$content = "Error in $file, line $line:\n\n" . print_r($message, true);
		$headers = array(
			'From: MEO Smart Capture <' . MAIL_USER . '>'
		);
		$hostname = function_exists('gethostname') ? gethostname() : php_uname('n');
		wp_mail( self::REPORT_ERRORS_TO, $hostname . ' :: error', $content, $headers );
	}
}
