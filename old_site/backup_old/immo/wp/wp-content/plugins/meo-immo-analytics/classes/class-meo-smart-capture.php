<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoSmartCapture {

	private $cf7integration = null;	 // MEO Smart Capture Contact Form 7 Integration instance
	private $utilities = null;		 // MEO Smart Capture Utilities instance
	private $downloadFormUrl = null; // File Request page URL

	public function __construct() {
		// Prepare filters, shortcodes and hooks
		$this->addHooks();
	}


	# Prepare specific filters and shortcodes
	public function addHooks() {
		
		$this->utilities = new MeoScCf7Utilities();
		$this->cf7integration = new MeoScCf7Integration($this, $this->utilities);
		
		// Contact Form 7
		$this->cf7integration->addHooks();
		
		// Smart Capture
		add_filter('attachment_link', array($this, 'convertPdfLinks'), 10, 2);
		add_shortcode('attachment_download_link', array($this, 'generateDownloadLink'));
		
		/* Shortcode to generate button price list [buttonPriceList url]
		 * [downloadButton url=""] */
		add_shortcode( 'downloadButton', array( $this, 'getDownloadButton' ) );
		
		
	}

	// Get File Request page url
	private function getDownloadFormUrl() {
		
		if (!empty($this->downloadFormUrl)) { 
			return $this->downloadFormUrl; 
		}

		$fileRequest_page_id = $this->utilities->getPostIdForTemplate( MEO_IMMO_ANALYTICS_ATTREQ_TPL_FRONT_NAME );

		if (empty($fileRequest_page_id)) 
		{ 
			return null; }

		$this->downloadFormUrl = get_permalink($fileRequest_page_id);
		
		return $this->downloadFormUrl;
	}

	// Get Download page url
	public function getDownloadLink($cf7, $posted_data, $file_id) {
		
		$downloader_id = $this->getDownloaderId($cf7, $posted_data);
		$download_code = $this->getDownloadCode($downloader_id, $file_id);

		$download_link_id = $this->utilities->getPostIdForTemplate( MEO_IMMO_ANALYTICS_ATTDLD_TPL_FRONT );
		$download_url = get_permalink( $download_link_id ) . '?email=' . urlencode($posted_data['email']) . '&download_code=' . urlencode($download_code).'&downloadlinkid='.$download_link_id.'&downloader_id='.$downloader_id;

		return $download_url;
	}

	// Get the contact id (the person who downloads)
	private function getDownloaderId($cf7, $posted_data) {
		
		global $wpdb;

		$downloader_id = $wpdb->get_var( $wpdb->prepare(
															"SELECT pd.id
															 	FROM ".$wpdb->prefix.MEO_ANALYTICS_FILEDLDER_TABLE." pd
															  	WHERE pd.email = %s",
																$posted_data['email']
														) );
		
		// New contact
		if (empty($downloader_id) || $downloader_id == 0 || !$downloader_id) {
			
			$values = array(
				'last_name'     => $posted_data['last_name'], 
				'first_name'    => $posted_data['first_name'],
				'email'         => $posted_data['email'],
				'phone'         => $posted_data['phone'],
				'address'       => $posted_data['address'],
				'postcode'      => $posted_data['postcode'],
				'city'          => $posted_data['city'],
				'country'       => ($posted_data['country']?$posted_data['country']:''),
				'language'      => substr($posted_data['_wpcf7_locale'], 0, 2),
				'creation_date' => current_time('mysql', 1)
			);
			
			$wpdb->insert( $wpdb->prefix.MEO_ANALYTICS_FILEDLDER_TABLE, $values );

			$downloader_id = $wpdb->insert_id;
			
			// Prepare / Schedule email to send with the new contact details
			$this->schedulePdfEmail($cf7, $posted_data);
			
		}
		// Existing contact
		else {
			// Don't send the form content to the salespeople if they know about this contact
			$cf7->skip_mail = true;
		}

		return $downloader_id;
	}

	// Tell the SmartCapture admin to send the salesman a PDF for this visitor
	private function schedulePdfEmail($cf7, $posted_data) {
		
		$smart_capture_url = get_option('smart_capture_url');
		$api_key = get_option('api_key');
		
		if (empty($smart_capture_url) || empty($api_key)) {
			// Will send the normal cf7 email
			return;
		}

		$analytics_id = $posted_data['analytics_id'];
		if (empty($analytics_id)) {
			// Try to get piwik analytics id from the cookie
			require_once( dirname(__FILE__) . '/class-meosc-piwik-integration.php');
			$analytics_id = MeoPiwikIntegration::mscPiwikGetAnalyticsIdFromCookie();
		}

		// We couldn't get the analytics id from the cookie
		if (empty($analytics_id)) {
			// Log an error
			require_once( dirname(__FILE__) . '/class-meo-error-handler.php');
			MeoErrorHandler::reportError(__FILE__, __LINE__, array(
				'message'      => "Analytics ID missing",
				'$cf7'         => $cf7,
				'$posted_data' => $posted_data
			));
			return;
		}

		// We found the analytics id, lets schedule the email to be sent
		$schedule_url = $smart_capture_url . "/wp/wp-admin/admin-ajax.php?action=schedule_pdf_email&analytics_id=" . $analytics_id . "&api_key=" . $api_key;

		$curl = new WP_Http_Curl();
		$response = $curl->request($schedule_url, array(
			'headers' => array(
				'Accept'       => 'application/json',
				'Content-type' => 'application/json',
			),
			'method' => 'GET',
			'redirection' => 0
		));

		// The call to smart capture failed.  Fall back to the normal email
		if ( is_wp_error( $response ) || empty( $response['body']) ) {
			return;
		}

		$schedule_data = json_decode($response['body']);

		if ($schedule_data->success) {
			// Don't send the cf7 email - the salespeople will get the PDF
			// Commented below [temporary] as we want to sent the c7 form with the new extra fields.
			// $cf7->skip_mail = true;
		}

		// The call to smart capture failed.  Fall back to the normal email
	}

	private function getDownloadCode($downloader_id, $file_id) {
		global $wpdb;
		$download_code = $wpdb->get_var('SELECT UUID_SHORT() as download_code');
		if (empty($download_code)) {
			$download_code = substr(str_shuffle("012345678901234567890123456789012345678901234567890123456789"), 0, 17);
		}

		$result = $wpdb->insert
		(
			$wpdb->prefix.MEO_ANALYTICS_FILEDL_TABLE,
			array(
				'file_downloader_id' => $downloader_id,
				'attachment_id'      => $file_id,
				'download_code'      => $download_code
			),
			array(
				'%d',
				'%d',
				'%s'
			)
		);

		if (false === $result) {
			return null;
		}

		return $download_code;
	}
	
	// Prepare link for file request
	public function convertPdfLinks($link, $post_id) {
		global  $download_lot_id, $post, $meoMIMEtypes;
	
		$attachment = get_post( $post_id );
	
		// Allowed MIME TYPES
		if (in_array($attachment->post_mime_type, $meoMIMEtypes)) {
				
			$download_form_url = $this->getDownloadFormUrl();
				
			if (!empty($download_form_url)) {
				$result = $download_form_url . '?file_id=' . $this->utilities->encodeAttachmentId($post_id) . '&post_id=' . ( empty($download_lot_id) ? $post->ID : $download_lot_id);
	
				return $result;
			}
		}
	
		return $link;
	}
	
	public function generateDownloadLink( $atts, $content = null ) {
		global $post;

		$atts = shortcode_atts( array(
			'attachment_id' => null,
			'class'         => null
		), $atts );

		$content = do_shortcode($content);
		$attachment_id = (int) $atts['attachment_id'];

		$link = '#';
		if (!empty($attachment_id)) {
			$link = $this->getDownloadFormUrl();
			$link .= '?file_id=' . $this->utilities->encodeAttachmentId( $attachment_id ) . '&post_id=' . $post->ID;

		}

		return '<a href="' . $link . '"' . ( $atts['class'] ? ' class = "' . $atts['class'] . '"' : '' ) . '>' . $content . '</a>';
	}
	
	
	# Build download button link
	/* [downloadButton fileid="" postid="" class="" text="" id=""] */
	public function getDownloadButton($atts) {
	
		global $post, $meoMIMEtypes;
	
		# Collect attributes
		$a = shortcode_atts( array(
		'fileRequest' => MEO_IMMO_ANALYTICS_ATTREQ_FRONT_SLUG,
		'fileid' => '',
		'postid' => '',
		'class' => '',
		'text' => '',
		'id' => 'downloadBtn'
				), $atts );
	
		$html = '';
		
		# Be sure we have something to download
		if($a['fileid'] && $a['fileid'] != ''){
			
			# Check if attachment exists
			$attachment = get_post( $a['fileid'] );
			
			if($attachment && in_array($attachment->post_mime_type, $meoMIMEtypes)) {
				
				# Post ID to pass to the system / Current by default
				if(!$a['postid'] || empty($a['postid']) || $a['postid'] == '') {
					$a['postid'] = $post->ID;
				}
					
				# Build File Request url
				$url = esc_url( home_url( '/' ) ).$a['fileRequest'].'/?file_id='.$this->utilities->encodeAttachmentId( $a['fileid'] ).'&post_id='.$a['postid'].'';
		
				if(!meo_crm_core_is_mobile()) {
		
					$a['class'] .= ' fancybox-iframe meo-immo-btn-download';
		
				} else {
					$current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
					$a['class'] .= '';
					$url .= '&current_url='.$current_url;
				}
		
				$html  = '<a id="'.$a['id'].'" class="'.$a['class'].'" href="'.$url.'">'.$a['text'].'</a>';
			
			}
	
		}
	
		return $html;
	}
	

	public function decodeAttachmentId($obfuscated_id) {
		return $this->utilities->decodeAttachmentId($obfuscated_id);
	}
}
